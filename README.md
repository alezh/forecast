# forecast

#### 介绍
销量预测

#### 软件架构
软件架构说明


#### 安装教程

1.  安装失败 conda 降级 conda install -n root conda=4.6
2.  xxxx
3.  xxxx

#### 新特征

每周支付转化率=（产生购买行为的客户人数 / 所有到达店铺的访客人数）× 100%
每周访问人数
每周下单人数
访客占比 = 


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


####  学习资料
1. https://slundberg.github.io/shap/notebooks/League%20of%20Legends%20Win%20Prediction%20with%20XGBoost.html
2. https://blog.csdn.net/u014084065/article/details/78553166
3. https://blog.csdn.net/phyllisyuell/article/details/81011660
4. https://blog.csdn.net/sinat_35512245/article/details/79668363
5. http://sofasofa.io/tutorials/shap_xgboost/
6. https://www.jianshu.com/p/fe47c70d31f9
7. https://aistudio.baidu.com/aistudio/projectdetail/127565
8. https://blog.csdn.net/kasdfu/article/details/103406168
9. https://aistudio.baidu.com/aistudio/projectdetail/174237
10. https://blog.csdn.net/PaddlePaddle/article/details/100059492
11. https://aistudio.baidu.com/aistudio/projectdetail/390914?forkThirdPart=1
12. https://aistudio.baidu.com/aistudio/projectdetail/224360
13. https://blog.csdn.net/PaddlePaddle/article/details/88290507
14. https://blog.csdn.net/u010159842/article/details/78053669
15. https://www.pianshen.com/article/3976142542/
16. https://www.freesion.com/article/6246223358/
17. https://www.cnblogs.com/Yanjy-OnlyOne/p/11288098.html
18. https://www.cnblogs.com/wj-1314/p/9402324.html
19. https://blog.csdn.net/a19990412/article/details/85139058
20. https://github.com/ShusenTang/Dive-into-DL-PyTorch
21. https://github.com/L1aoXingyu/code-of-learn-deep-learning-with-pytorch
22. https://blog.csdn.net/hjxzb/article/details/78610961
23. https://www.jianshu.com/p/34253fd829a3
24. https://www.7forz.com/3319/
27. https://blog.csdn.net/huangqihao723/article/details/105321168/
28. https://blog.csdn.net/qq_23144435/article/details/89810816
29. https://cloud.tencent.com/developer/news/319575
30. https://github.com/L1aoXingyu/code-of-learn-deep-learning-with-pytorch/blob/master/chapter5_RNN/time-series/lstm-time-series.ipynb
31. https://zhuanlan.zhihu.com/p/94757947
32. https://www.pypandas.cn/docs/user_guide/indexing.html

## pytorch
#### 数据标准化（归一化）
1. https://zhuanlan.zhihu.com/p/69431151  
2. https://www.pytorchtutorial.com/5-4-batch-normalization/  
3. https://www.jianshu.com/p/bf0d6ad12547

#### tensorboardX
1. https://blog.csdn.net/bigbennyguo/article/details/87956434
2. https://www.cnblogs.com/yongjieShi/p/9495564.html

#### 其他问题
1. https://www.zhihu.com/question/67209417
2. https://blog.csdn.net/tefuirnever/article/details/94122670

### 多模型合并项目
1. https://mp.weixin.qq.com/s/Yix0xVp2SiqaAcuS6Q049g
2. https://cloud.tencent.com/developer/article/1371508
3. https://cloud.tencent.com/developer/article/1528346

### 其他预测
1. https://github.com/Jenniferz28/Time-Series-ARIMA-XGBOOST-RNN
2. https://blog.csdn.net/zwqjoy/article/details/86490098
~~~~

店铺与对应ID
库存：正常+锁定
库存状态：差异锁定 出货在途 待调 待损 待退 待销售 其他 锁定 预留 正常
订单状态：拆单关闭 寻源结束 已发货 已发货(财务已审核) 已发货(承运商已审) 已发货(初始) 已取消 已审核 已审核(财务已审核) 已审核(承运商已审) 已审核(初始) 已审核(客服已审核) 已审核(物流已审核) 
          已提交(初始) 已完成 已作废 已作废(财务已审核) 已作废(承运商已审) 已作废(初始) 已作废(客服已审核) 已作废(物流已审核) 异常
