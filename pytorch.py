import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader

from net.models import GRUNet, LSTMNet

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class TrainSet(Dataset):
    def __init__(self, data):
        # 定义好 image 的路径
        self.data, self.label = data[:, :-1].float(), data[:, -1].float()

    def __getitem__(self, index):
        return self.data[index], self.label[index]

    def __len__(self):
        return len(self.data)


cols = ['discount', 'hd', 'hao', 'ping', 'cha', 'hf', 'avghao', 'avgping', 'avgcha', 'avghf', 'pv', 'uv',
        'sc', 'jg', 'at', 'sh', 'WeeksAgoNum', 'twoWeeksAgoNum', 'num']
Data = pd.read_csv('cache/daily.csv')[cols].fillna(0)
# 归一化 min-max标准化
Data = Data.apply(lambda x: (x - min(x)) / (max(x) - min(x)))
# Z-score标准化方法
# Data = Data.apply(lambda x: (x - np.mean(x)) / (np.std(x)))
train_len = int(len(Data) * 0.8)
train_x = Data[:train_len]
test = Data[train_len:].copy()

train = torch.tensor(train_x.values, device=device, dtype=torch.float32)

n = len(cols) - 1  # n为模型中的n
LR = 0.0001  # LR是模型的学习率
EPOCH = 10  # EPOCH是多次循环
batch_size = 100

trainset = TrainSet(train)
# 每次输出10个数据
trainloader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

net = LSTMNet(n).cuda()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(net.parameters(), lr=LR, weight_decay=0.001)
# total_loss = []
# start training
for e in range(EPOCH):
    for i, (X, y) in enumerate(trainloader):
        # var_x = Variable(torch.unsqueeze(X, dim=0))
        var_x = X.view(-1, 1, X.shape[1])
        var_y = y
        # forward
        out = net(var_x)
        loss = criterion(torch.squeeze(out), y)
        # loss = criterion(out, y)
        # backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print('Epoch: {}, Loss: {:.5f}'.format(e + 1, loss.item()))
        # if (e + 1) % batch_size == 0:  # 每 batch_size 次输出结果
        #     torch.save(obj=net.state_dict(), f='models/lstmnetpro_gru_%d.pth' % (e + 1))
            # total_loss.append(loss.item())
torch.save(obj=net.state_dict(), f="models/lstmnet_gru_1000.pth")

# 预测
# net.eval()
# test_x = torch.tensor(test.iloc[:20, :-1].copy().values, device=device, dtype=torch.float32)
# test_y = test.iloc[:20, -1].copy()
#
# pres = net(test_x.view(-1, 1, test_x.shape[1]))
# p = torch.squeeze(pres.cpu()).detach().numpy()
#
# test_true_data = pd.DataFrame(data={'actual': test_y.values})
# test_pred_data = pd.DataFrame(data={'prediction': p})
# plt.figure()
# test_pred_data.prediction.plot(color='red', label='Predict', figsize=(12, 8))
# test_true_data.actual.plot(color='blue', label='Original', figsize=(12, 8))
# # plt.title('RMSE: %.4f' % np.sqrt(sum((test[y_key] - result[y_key]) ** 2) / result[y_key].size))
# plt.legend(loc='best')  # 将样例显示出来
# plt.grid(True)
# plt.show()
