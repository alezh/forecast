import warnings
import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from learning.lgb import l_model
from learning.lstm import LSTM_Regression, LSTM_Torch
from learning.xgb import x_model
# warnings.filterwarnings('ignore')
from entity.dataSource import DataSource

# DataSource().spu_class(True)
# DataSource().SourceToCsv()
# DataSource().GetMonthMerge()
train_x, train_y, test_x, test_y = DataSource().GetTrainClassDataDay('T恤类')
# train_x, valid_x, train_y, valid_y = train_test_split(train_x, train_y, test_size=0.2, random_state=12)
# T恤类 周期性 12个月
# train_x, train_y, test_x, test_y = DataSource().GetTrainData('AA060243')
submission, importances, metric, train_sub = x_model(train_x, train_y, test_x, test_y)

# train, test = DataSource().save(train_sub, submission, 'AA060243', s=True)
# app_train_test = [train, test]
# app_train_test = pd.concat(app_train_test)
# train_x, train_y, test_x, test_y = DataSource().GetTrainData('AA060243', app_train_test)
# train_x = train_x.drop(columns='xgb')
# test_x = test_x.drop(columns='xgb')
# submission, importance, m, train_sub = l_model(train_x, train_y, test_x, test_y)
# LSTM_Regression(train_x, train_y, test_x, test_y)
plt.figure()
submission.num.plot(color='red', label='Predict', figsize=(20, 8))
test_y.plot(color='blue', label='Original', figsize=(20, 8))
# plt.title('MAPE: %.4f' % np.sqrt(sum((test_y - submission.num) ** 2) / submission.num.size))
# plt.title(sum(abs(test_y - submission.num)) * 100 / (test_y.size * sum(test_y.values)))
plt.title(sum(abs((submission.num - test_y) / test_y)) / test_y.size * 100)
plt.legend(loc='best')  # 将样例显示出来
plt.grid(True)
plt.show()
