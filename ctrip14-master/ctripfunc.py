import pandas as pd
import numpy as np
import lightgbm as lgb
from sklearn.ensemble import BaggingRegressor
import calendar
from datetime import datetime


def addDateInterval(date):
    """
    返回当前日期为从2000年1月开始的第几个月
    :dat str
        'YYYY-MM-DD'
    :return int
    """
    if date == '-1':
        return -1
    _date = pd.to_datetime(date)
    if _date.year < 2000:
        return -1
    month_interval = (_date.year - 2000) * 12 + _date.month - 1
    return month_interval


def addVotersDiscretization(x):
    """
    对voters属性进行6级离散化处理，缺失值为-1
    :x int
    :return int
    """
    if x > -1:
        if x < 100:
            return 0
        elif x < 500:
            return 1
        elif x < 1000:
            return 2
        elif x < 2500:
            return 3
        elif x < 10000:
            return 4
        else:
            return 5
    return -1


def addMeanPrice(product_quantity, product_info):
    """
    添加平均售价
    """
    new_pq = product_quantity[product_quantity.price != -1].copy()
    new_pq['sum'] = new_pq['price'] * new_pq['ciiquantity']
    sumc = new_pq.groupby('product_id')['ciiquantity'].sum()
    product_info.loc[product_info.index.isin(new_pq.product_id.unique()), 'price'] = (
            new_pq.groupby('product_id')['sum'].sum() / sumc)
    product_info.fillna(-1, inplace=True)
    return product_info


def addEval0(col):
    """
    根据评分人数和客户评分计算新的评级特征
    :col ['voters','eval3']
    :reuturn int
    """
    voters = col[0]
    eval3 = col[1]
    if voters < 0:
        return -1
    if voters > 2:
        if eval3 > 4:
            return 2
        elif eval3 < 3:
            return 0
    return 1


def transformProductInfo(product_info, product_quantity):
    product_info['index'] = product_info['product_id']
    product_info.set_index('index', inplace=True)
    product_info['startdate'] = product_info['startdate'].apply(
        addDateInterval)
    product_info['upgradedate'] = product_info['upgradedate'].apply(
        addDateInterval)
    product_info['cooperatedate'] = product_info['cooperatedate'].apply(
        addDateInterval)

    # 订单属性1 我们发现订单属性1对于产品来说其实是一个唯一属性,将其添加进info表
    oa1 = product_quantity.drop_duplicates('product_id')
    product_info.loc[oa1['product_id'],
                     'orderattribute1'] = oa1['orderattribute1'].tolist()
    product_info.fillna(-1, inplace=True)

    product_info['voters'] = product_info['voters'].apply(
        addVotersDiscretization)
    product_info = addMeanPrice(product_quantity, product_info)
    product_info['eval0'] = product_info[[
        'voters', 'eval3']].apply(addEval0, axis=1)

    return product_info


def change_dat2(col):
    """
    计算当前月份到产品开售月份(start_date)的月数
    : col   ['startdate','year','month']
    : return int
    """
    start_date, year, month = col
    current_date = addDateInterval('-'.join([str(year), str(month).zfill(2)]))
    if start_date > current_date:
        return -1
    return current_date - start_date


def get_holiday(col):
    '''
    计算指定年月的假期天数
    : col ['year','month']
    : return int
    '''
    holiday = [9, 11, 10, 9, 10, 10, 8, 10, 8, 12, 10, 8,
               10, 11, 9, 9, 11, 8, 8, 10, 9, 13, 9, 8,
               11, 11, 8, 10, 10, 9, 10, 8, 9, 13, 8, 9,
               12]
    year, month = col
    return holiday[(year - 2014) * 12 + month - 1]


def get_x(quantity, product_info):
    """
    根据product_id product_month 生成训练数据集
    : quantity dataframe
    : product_info datafrmae
    : return dataframe
    """
    x = product_info.loc[quantity['product_id']].copy()
    x.reset_index(drop=True, inplace=True)
    x['year'] = quantity['product_month'].str[:4].apply(int)
    x['month'] = quantity['product_month'].str[5:7].apply(int)
    x['startdate'] = x[['startdate', 'year', 'month']].apply(
        change_dat2, axis=1)
    x['upgradedate'] = x[['upgradedate', 'year', 'month']].apply(
        change_dat2, axis=1)
    x['cooperatedate'] = x[['cooperatedate', 'year', 'month']].apply(
        change_dat2, axis=1)
    x['holiday'] = x[['year', 'month']].apply(get_holiday, axis=1)
    x = pd.get_dummies(x, columns=['month'])
    x['month79'] = 1 - (1 - x['month_7']) * (1 - x['month_9'])

    return x


def main():
    # 读取数据
    data_path = './'
    product_quantity = pd.read_csv('product_quantity.txt')
    product_info = pd.read_csv('product_info.txt', )

    # 特征处理

    product_info = transformProductInfo(product_info, product_quantity)

    # 将所有商品每个月的销量转化为一个37*4001的数组供之后处理负数预测值的时候使用
    product_quantity['product_date'] = product_quantity['product_date'].str[:7]
    quantity = product_quantity.groupby(['product_id', 'product_date']).sum()
    quantity.reset_index(inplace=True)

    quantity_arr = np.full((37, 4001), -1, dtype=np.int32)
    for idx in quantity.index:
        pid = quantity.loc[idx, 'product_id']
        date = quantity.loc[idx, 'product_date']
        quantity_arr[(int(date[:4]) - 2014) * 12 + int(date[5:7]) -
                     1][pid] = quantity.loc[idx, 'ciiquantity']

    train_y = quantity['ciiquantity'].tolist()

    quantity.rename(columns={'product_date': 'product_month'}, inplace=True)
    train_x = get_x(quantity, product_info)

    # 模型训练

    print('training start')
    num_clfs = 4
    clfs = []
    for i in range(num_clfs):
        clf = BaggingRegressor(lgb.sklearn.LGBMRegressor(max_depth=10, n_estimators=2000, num_leaves=80 + 10 * i, ),
                               n_estimators=6, random_state=i, n_jobs=1, max_samples=0.8 + 0.001 * i, )
        clf.fit(train_x, train_y, )
        clfs.append(clf)
    print('training  done')

    # 执行预测
    print('predicting start')
    out = pd.read_csv(data_path + 'prediction_lilei_20170320.txt')
    test_x = get_x(out[['product_id', 'product_month']], product_info)
    ans = [clf.predict(test_x) for clf in clfs]
    # 23个月均无数据的product_id
    invalid_pid = [i for i in range(1, 4001) if quantity_arr[:23, i].max() < 0]
    # 将负数的预测改为前23个月有效的最小值,若无则为0
    history_min = [0] * 4001
    for i in range(1, 4001):
        quantity_i = quantity_arr[:23, i]
        if quantity_i.max() < 0:
            history_min[i] = 0
        else:
            history_min[i] = quantity_i[quantity_i > -1].min()
    # 生成最后提交结果
    final_out = out.copy()
    final_out['ciiquantity_month'] = 0
    for pred_y in ans:
        out['ciiquantity_month'] = pred_y
        # 将23个月均无数据且startdate,cooperatedate均在第24个月以后的清0
        for pid in invalid_pid:
            product_info_i = product_info.loc[pid]
            dat = min(product_info_i['startdate'],
                      product_info_i['cooperatedate']) - 191 + 23
            for j in range(23, dat.astype(np.int)):
                out.loc[(j - 23) * 4000 + pid - 1, 'ciiquantity_month'] = 0

        idx = out['ciiquantity_month'] < 0
        out.loc[idx, 'ciiquantity_month'] = np.array(
            history_min)[out.loc[idx, 'product_id']]
        final_out['ciiquantity_month'] += out['ciiquantity_month']
    out['ciiquantity_month'] = final_out['ciiquantity_month'] / len(ans)

    out.to_csv('l_bg46_lgb100_-1first.txt', index=False)
    print('predicting done')


if __name__ == "__main__":
    main()
