#!/usr/bin/env python
# coding: utf-8



import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import GridSearchCV




# 读取数据

stat_004 = pd.read_excel("../data/统计004.xlsx")
stat_012 = pd.read_excel("../data/统计012.xlsx")
stat_015 = pd.read_excel("../data/统计015.xlsx")
stat_017 = pd.read_excel("../data/统计017.xlsx")
stat_018 = pd.read_excel("../data/统计018.xlsx")





# 定义一个模型训练函数（其中SVM预测效果很差）
def train_model(X_train, y_train, X_test, y_test):
    for model in ["svm", "rf"]:
        if model == "svm":
            print("----------------SVM----------------")

            # 用默认的参数训练模型
            reg = SVR()
            reg.fit(X_train, y_train)

        else:
            print("-------------randomforest-----------")
            # 训练模型
            reg = RandomForestRegressor(random_state=0, n_jobs=-1)
            reg.fit(X_train, y_train)
        # 预测
        y_pred_train = reg.predict(X_train)
        y_pred_test = reg.predict(X_test)
        # 计算rmse
        rmse_train = np.sqrt(mean_squared_error(y_train, y_pred_train))
        print("训练集rmse={}".format(rmse_train))
        rmse_test = np.sqrt(mean_squared_error(y_test, y_pred_test))
        print("测试集rmse={}".format(rmse_test))
        # 计算mape
        mape_train = sum(abs(y_train - y_pred_train)) * 100 / (len(y_train) * sum(y_train))
        print("训练集mape={}".format(mape_train))
        mape_test = sum(abs(y_test - y_pred_test)) * 100 / (len(y_test) * sum(y_test))
        print("测试集mape={}".format(mape_test))
        print("-------------pred_train-----------")
        print(y_pred_train)
        print("-------------pred_text-----------")
        print(y_pred_test)




##1 对卫裤建模


#对stat_004取2019年后四周作为测试集，其余作为训练集进行建模（数据量较少，一般测试集取数据量百分之10左右）
logi1=(stat_004['年份']==2019)&(stat_004['周次']<=47)
X_train1=(stat_004[logi1].drop(columns='销量')).copy()
y_train1=stat_004[logi1]['销量'].copy()
X_test1=stat_004[~logi1].drop(columns='销量').copy()
y_test1=stat_004[~logi1]['销量']
#删除年份字段
del X_train1['年份']
del X_train1['周次']
del X_test1['年份']
del X_test1['周次']

#建立SVM和rf模型
train_model(X_train1,y_train1,X_test1,y_test1)





##2 对鞋子进行建模预测销量
#对stat_012取2019年后四周作为测试集，其余作为训练集进行建模（数据量较少，一般测试集取数据量百分之10左右）

logi2=(stat_012['年份']==2019)&(stat_012['周次']<=47)

X_train2=(stat_012[logi2].drop(columns='销量')).copy()
y_train2=stat_012[logi2]['销量'].copy()

X_test2=stat_012[~logi2].drop(columns='销量').copy()
y_test2=stat_012[~logi2]['销量']

#删除年份字段
del X_train2['年份']
del X_train2['周次']
del X_test2['年份']
del X_test2['周次']

#建立SVM和rf模型
train_model(X_train2,y_train2,X_test2,y_test2)




##3 对卫衣建模

#对stat_015取2019年全年作为测试集和训练集（015数据集后半年销量绝大部分为0，所以不便划分测试集）
logi3=(stat_015['年份']==2019)&(stat_015['周次']<=52)
X_train3=(stat_015[logi3].drop(columns='销量')).copy()
y_train3=stat_015[logi3]['销量'].copy()
X_test3=stat_015[logi3].drop(columns='销量').copy()
y_test3=stat_015[logi3]['销量']
#删除年份字段
del X_train3['年份']
del X_train3['周次']
del X_test3['年份']
del X_test3['周次']

#建立SVM和rf模型
train_model(X_train3,y_train3,X_test3,y_test3)








##4 对运动裤建模


#对stat_017取2019年后四周作为测试集，其余作为训练集进行建模（数据量较少，一般测试集取数据量百分之10左右）
logi4=(stat_017['年份']==2019)&(stat_017['周次']<=47)
X_train4=(stat_017[logi4].drop(columns='销量')).copy()
y_train4=stat_017[logi4]['销量'].copy()
X_test4=stat_017[~logi4].drop(columns='销量').copy()
y_test4=stat_017[~logi4]['销量']
#删除年份字段
del X_train4['年份']
del X_train4['周次']
del X_test4['年份']
del X_test4['周次']

#建立SVM和rf模型
train_model(X_train4,y_train4,X_test4,y_test4)



##5 对童装进行建模预测销量
#对stat_018取2019年后四周作为测试集，其余作为训练集进行建模（数据量较少，一般测试集取数据量百分之10左右）

logi5=(stat_018['年份']==2019)&(stat_018['周次']<=47)

X_train5=(stat_018[logi5].drop(columns='销量')).copy()
y_train5=stat_018[logi5]['销量'].copy()

X_test5=stat_018[~logi5].drop(columns='销量').copy()
y_test5=stat_018[~logi5]['销量']

#删除年份字段
del X_train5['年份']
del X_train5['周次']
del X_test5['年份']
del X_test5['周次']

#建立SVM和rf模型
train_model(X_train5,y_train5,X_test5,y_test5)
