import pandas as pd
import xgboost as xgb
import numpy as np

alldata = pd.read_excel('../data/统计018.xlsx')

j = len(alldata)
i = 1
while (2 * i + 4) <= j:
    x = 2 * i + 2
    y = 2 * i + 4
    col = ['真实pv_week', 'uv_week', '收藏数_week', '加购数_week', '平均停留时长_week', '搜索引导访客数_week',
           '大促次数', '优惠力度']
    train_x = alldata[:x].copy()[col]
    label_x = alldata.iloc[:x, 2]
    test = alldata[x:y].copy()[col]
    test_label = alldata[x:y]['销量']
    train_val = xgb.DMatrix(train_x, label=label_x)
    test_val = xgb.DMatrix(test)
    param = {'max_depth': 5, 'eta': 0.1, 'nthread': 4}
    # param['objective'] = 'binary:logistic'
    model = xgb.train(param, train_val,num_boost_round=50)
    # 预测值
    preds = model.predict(test_val)
    # 输出模型各变量的权重
    # importances = model.feature_importances_
    rmse = 'RMSE: %.4f' % np.sqrt(sum((preds - test_label.values) ** 2) / test_label.size)
    mape = sum(abs(test_label.values - preds)) * 100 / (test_label.size * sum(test_label.values))
    i = i + 1
    print('MAPE',mape)

# if __name__ == '__main__':
#     pass
