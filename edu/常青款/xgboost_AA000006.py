import pandas as pd
import numpy as np
import xgboost as xgb
import matplotlib.pyplot as plt

alldata = pd.read_csv('aa00006not11.csv')
train = alldata[:128]
test = alldata[128:].copy()
x_org = test.copy()
train_sale = train['sale']
test_sale = test['sale'].values
col = ["wpv", 'wuv', 'wsc', 'wjg', 'wtl', 'wtc', 'wfk', 'hps', 'zps', 'cps', 'pls', 'hpl', 'hfl', 'one', 'two', 'three',
       'promotion', 'yhje', 't']
train_v = xgb.DMatrix(train[col], label=train_sale)
test_v = xgb.DMatrix(test[col])
param = {'max_depth': 5, 'eta': 0.1}
model = xgb.train(param, train_v,num_boost_round=50)
preds = model.predict(test_v)
test['sale'] = preds

mape = sum(abs((test_sale - preds) / test_sale)) / len(test_sale) * 100
mape2 = sum(abs((test_sale - preds))) / sum(test_sale) * 100

true_data = pd.DataFrame(data={'date': x_org['week'], 'actual': x_org['sale'].values})
predictions_data = pd.DataFrame(data={'date': test['week'], 'prediction': test['sale'].values})
plt.figure()
plt.plot(true_data['date'], true_data['actual'], 'go-', label='true value')
plt.plot(predictions_data['date'], predictions_data['prediction'], 'ro-', label='predict value')
plt.title('RF_MAPE: %f' % mape)
plt.legend()  # 将样例显示出来
plt.show()
