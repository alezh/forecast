import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error,explained_variance_score,mean_absolute_error,r2_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor

#读取训练集和测试集
test=pd.read_csv('D_test.csv',index_col=0,encoding='gbk')
train=pd.read_csv('D_train.csv',index_col=0,encoding='gbk')
index1=train.index
index2=test.index
#确定要预测的变量和考虑的因素
X_train=train[['wpv', 'wuv', 'wsc', 'wjg', 'wtl', 'wtc', 'wfk', 'hps', 'zps', 'cps','pls', 'hpl', 'hfl', 'one', 'two', 'three', 'promotion','yhje']]
X_train=np.array(X_train)
X_test=test[['wpv', 'wuv', 'wsc', 'wjg', 'wtl', 'wtc', 'wfk', 'hps', 'zps', 'cps','pls', 'hpl', 'hfl', 'one', 'two', 'three', 'promotion','yhje']]
X_test=np.array(X_test)
y_train=train['w_sale']
y_train=np.array(y_train)
y_test=test['w_sale']
y_test=np.array(y_test)


##参数选择
criterion=['mae','mse']
#决策树属性['gini','entropy']   回归：mae,mse
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
max_features = ['auto', 'sqrt']
max_depth = [int(x) for x in np.linspace(10, 100, num = 10)]
max_depth.append(None)
min_samples_split = [2, 5, 10]
min_samples_leaf = [1, 2, 4]
bootstrap = [True, False]
random_grid = {'criterion':criterion,'n_estimators': n_estimators,'max_features': max_features,'max_depth': max_depth,'min_samples_split': min_samples_split,'min_samples_leaf': min_samples_leaf,'bootstrap': bootstrap}
##构建模型 随机森林
clf= RandomForestRegressor()
clf_random = RandomizedSearchCV(estimator=clf, param_distributions=random_grid,n_iter = 10,cv = 3, verbose=2, random_state=42, n_jobs=1)
#回归
clf_random.fit(X_train,y_train)
print (clf_random.best_params_)#输出最优参数

#根据输出的最优参数设置模型
rf=RandomForestRegressor(criterion='mse',bootstrap=True,max_features='sqrt',max_depth=100,min_samples_split=10,n_estimators=600,min_samples_leaf=4) 
rf.fit(X_train,y_train)
#预测
y_train_pred=rf.predict(X_train)
y_test_pred=rf.predict(X_test)


#输出评价指标
print ("决策树模型评估--训练集：")
print ('训练r^2:',rf.score(X_train,y_train))
print ('均方差',mean_squared_error(y_train,y_train_pred))
print ('绝对差',mean_absolute_error(y_train,y_train_pred))
print ('解释度',explained_variance_score(y_train,y_train_pred))
print('RMSE: %.4f'% np.sqrt(sum((y_train_pred-y_train)**2)/y_train.size))
print('MAPE:',sum(abs((y_train_pred-y_train)/y_train))/y_train.size*100)

print ("决策树模型评估--测试集：")
print ('验证r^2:',rf.score(X_test,y_test))
print ('均方差',mean_squared_error(y_test,y_test_pred))
print ('绝对差',mean_absolute_error(y_test,y_test_pred))
print ('解释度',explained_variance_score(y_test,y_test_pred))
print('RMSE: %.4f'% np.sqrt(sum((y_test_pred-y_test)**2)/y_test.size))
print('MAPE:',sum(abs((y_test_pred-y_test)/y_test))/y_test.size*100)



#显示训练集拟合结果图
d1=train.week
train_true_data = pd.DataFrame(data={'date': d1, 'actual': y_train})
train_pred_data = pd.DataFrame(data={'date':d1 , 'prediction': y_train_pred})
plt.figure()
train_pred_data.plot(color='red', label='Predict',figsize=(12,8))
train_true_data.actual.plot(color='blue', label='Original',figsize=(12,8))
plt.title('RMSE: %.4f'% np.sqrt(sum((y_train_pred-y_train)**2)/y_train.size))
plt.legend(loc='best')        # 将样例显示出来
plt.grid(True)
plt.show()
#显示测试集预测结果图
d2=test.week
test_true_data = pd.DataFrame(data={'date': d2, 'actual': y_test})
test_pred_data = pd.DataFrame(data={'date':d2 , 'prediction': y_test_pred})
plt.figure()
test_pred_data.plot(color='red', label='Predict',figsize=(12,8))
test_true_data.actual.plot(color='blue', label='Original',figsize=(12,8))
plt.title('RMSE: %.4f'% np.sqrt(sum((y_test_pred-y_test)**2)/y_test.size))
plt.legend(loc='best')        # 将样例显示出来
plt.grid(True)
plt.show()