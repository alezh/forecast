import pandas as pd
import numpy as np
import xgboost as xgb
import matplotlib.pyplot as plt

alldata = pd.read_excel('AA000013.xlsx')

train = alldata[:140]
test = alldata[140:].copy()
x_org = alldata[140:]
train_sale = train['sales']
test_sale = test.copy()
col = ["mpv","muv",'周收藏数','周加购数','周差评数','周回复率','前一周销量','周促销天数','优惠单价','前两周销量','前三周销量']
train_v = xgb.DMatrix(train[col], label=train_sale)
test_v = xgb.DMatrix(test[col])
param = {'max_depth': 5, 'eta': 0.1}
model = xgb.train(param, train_v)
preds = model.predict(test_v)
test['sales'] = preds

mape = sum(abs((test_sale['sales'] - preds) / test_sale['sales'])) / len(test_sale) * 100
mape2 = sum(abs((test_sale['sales'] - preds))) / sum(test_sale['sales']) * 100

true_data = pd.DataFrame(data={'date': x_org['monday'], 'actual': x_org['sales'].values})
predictions_data = pd.DataFrame(data={'date': test['monday'], 'prediction': test['sales'].values})
plt.figure()
plt.plot(true_data['date'], true_data['actual'], 'go-', label='true value')
plt.plot(predictions_data['date'], predictions_data['prediction'], 'ro-', label='predict value')
plt.title('RF_MAPE: %f' % mape)
plt.legend()  # 将样例显示出来
plt.show()
