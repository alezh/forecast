# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime


###########1.读取数据部分##########
def load_data(Path1,Path2):

    train = pd.read_csv(Path1,encoding='gb2312')

    test = pd.read_csv(Path2,encoding='gb2312')
    train_x = train[["wpv","wuv","周收藏数","周加购数","周平均停留时长","周平均页面跳出率","周搜索引导访客数","周好评数","周差评数","周好评率","周答复率","prew1","prew2","prew3","周优惠总额"]]
    train_y = train["wsales"]
    test_x = test[["wpv","wuv","周收藏数","周加购数","周平均停留时长","周平均页面跳出率","周搜索引导访客数","周好评数","周差评数","周好评率","周答复率","prew1","prew2","prew3","周优惠总额"]]
    test_y = test["wsales"]
    train_x = np.array(train_x)
    train_y = np.array(train_y)
    test_x = np.array(test_x)
    test_y = np.array(test_y)
    dates = test['time']
    dates = [datetime.strptime(date, '%Y/%m/%d') for date in dates]
    return train_x, train_y, test_x, test_y, dates
###########2.回归部分##########
def regression_method(model,dates):
    model.fit(x_train,y_train)
    score = model.score(x_test, y_test)
    result = model.predict(x_test)
    #result_T = model.predict(x_train)
    ResidualSquare = (result - y_test)**2     #计算残差平方
    #ResidualSquare_T = (result_T - y_train)**2
    RSS = sum(ResidualSquare)   #计算残差平方和
    #RSS_T = sum(ResidualSquare_T)
    MSE = np.mean(ResidualSquare)       #计算均方差
    #MSE_T = np.mean(ResidualSquare_T)
    RMSE = MSE ** 0.5
    #RMSE_T = MSE_T ** 0.5
    MAPE=sum(abs((y_test - result)/y_test))/y_test.size*100
    #MAPE_T=sum(abs((y_train - result_T)/y_train))/y_train.size*100
    num_regress = len(result)   #回归样本个数
    
    print(f'n={num_regress}')
    print(f'R^2={score}')
    print(f'MSE={MSE}')
    print(f'RMSE={RMSE}')
    print(f'RSS={RSS}')
    print(f'MAPE={MAPE}')
    #print(f'RMSE_T={RMSE_T}')
    #print(f'MAPE_T={MAPE_T}')

    true_data = pd.DataFrame(data={'date': dates, 'actual': y_test})
    predictions_data = pd.DataFrame(data={'date':dates , 'prediction': result})

############绘制折线图##########
    plt.figure()
    plt.plot(true_data['date'], true_data['actual'],'go-',label='true value')
    plt.plot(predictions_data['date'],predictions_data['prediction'],'ro-',label='predict value')
    plt.title('RF_MAPE: %f'% MAPE)
    plt.legend()        # 将样例显示出来
    plt.show()
    return result


##########3.绘制验证散点图########
def scatter_plot(TureValues,PredictValues):
    #设置参考的1：1虚线参数
    xxx = [-0.5,1.5]
    yyy = [-0.5,1.5]
    #绘图
    plt.figure()
    plt.plot(xxx , yyy , c='0' , linewidth=1 , linestyle=':' , marker='.' , alpha=0.3)#绘制虚线
    plt.scatter(TureValues , PredictValues , s=20 , c='r' , edgecolors='k' , marker='o' , alpha=0.8)#绘制散点图，横轴是真实值，竖轴是预测值
    plt.xlim((0,1))   #设置坐标轴范围
    plt.ylim((0,1))
    plt.title('RandomForestRegressionScatterPlot')
    plt.show()


###########4.预设回归方法##########
####随机森林回归####
from sklearn import ensemble
model_RandomForestRegressor = ensemble.RandomForestRegressor(n_estimators=800)   #esitimators决策树数量


########5.设置参数与执行部分#############
#设置数据参数部分
x_train , y_train , x_test , y_test ,dates= load_data('train2_pre4w.csv','test2_pre4w.csv')
y_pred = regression_method(model_RandomForestRegressor,dates)        #括号内填上方法，并获取预测值
#scatter_plot(y_test,y_pred)  #生成散点图

