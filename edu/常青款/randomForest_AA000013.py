import pandas as pd
import numpy as np
from sklearn import ensemble
import matplotlib.pyplot as plt

alldata = pd.read_excel('AA000013.xlsx')
train = alldata[:140]
test = alldata[140:].copy()
x_org = alldata[140:]
col = ['mpv', 'muv', '周收藏数', '周加购数', '周好评数', '周差评数', '周好评率', '周回复率', '前一周销量',
       '前两周销量', '前三周销量', '周促销天数', '优惠单价']
model_regressor = ensemble.RandomForestRegressor(n_estimators=800)
model_regressor.fit(train[col], train['sales'])
model_regressor.score(test[col], x_org['sales'])
# 预测和集
result = model_regressor.predict(test[col])
test['sales'] = result

rmse = 'RMSE: %.4f' % np.sqrt(sum((result - x_org['sales']) ** 2) / x_org['sales'].size)
mape = sum(abs(x_org['sales'] - result)) * 100 / x_org['sales'].size * 100

true_data = pd.DataFrame(data={'date': x_org['monday'], 'actual': x_org['sales'].values})
predictions_data = pd.DataFrame(data={'date': test['monday'], 'prediction': test['sales'].values})
plt.figure()
plt.plot(true_data['date'], true_data['actual'], 'go-', label='true value')
plt.plot(predictions_data['date'], predictions_data['prediction'], 'ro-', label='predict value')
plt.title('RF_MAPE: %f' % mape)
plt.legend()  # 将样例显示出来
plt.show()
