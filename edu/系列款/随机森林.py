import pandas as pd
import numpy as np
from sklearn import ensemble
import matplotlib.pyplot as plt

allData = pd.read_csv('data011.csv')
j = len(allData['销量'])
i = 1
while (2 * i + 6) <= j:
    x = 2 * i + 2
    y = 2 * i + 4
    col = ["pv", "uv", "收藏数", "加购数", "平均停留时长", "搜索引导访客数", "大促次数", "优惠力度", "库存", "好评数", "差评数",
           "累计好评率", "累计差评率"]
    train_val = allData[:x]
    test = allData[x:y].copy()
    label_x = train_val['销量']
    test_val = allData[x:y].copy()

    model_regressor = ensemble.RandomForestRegressor(n_estimators=800)
    model_regressor.fit(train_val[col], label_x)
    model_regressor.score(test_val[col], test['销量'])
    # 预测和集
    result = model_regressor.predict(test_val[col])
    test_val['pred'] = result

    rmse = 'RMSE: %.4f' % np.sqrt(sum((result - test['销量'].values) ** 2) / test['销量'].size)
    mape = sum(abs(test['销量'].values - result)) * 100 / (len(test['销量']) * sum(test['销量']))
    i = i + 1
    print("MAPE", mape)

    # true_data = pd.DataFrame(data={'date': test['周'], 'actual': test['销量'].values})
    # predictions_data = pd.DataFrame(data={'date': test_val['周'], 'prediction': test_val['pred'].values})
    # plt.clf()
    # plt.figure()
    # plt.plot(true_data['date'], true_data['actual'], 'go-', label='true value')
    # plt.plot(predictions_data['date'], predictions_data['prediction'], 'ro-', label='predict value')
    # plt.title('RF_MAPE: %f' % mape)
    # plt.legend()  # 将样例显示出来
    # plt.show()
