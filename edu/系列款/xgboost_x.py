import pandas as pd
import numpy as np
import xgboost as xgb

allData = pd.read_csv('data011.csv')
j = len(allData['销量'])
i = 1
while (2 * i + 6) <= j:
    x = 2 * i + 2
    y = 2 * i + 4
    col = ["pv", "uv", "收藏数", "加购数", "平均停留时长", "搜索引导访客数", "大促次数", "优惠力度", "库存", "好评数", "差评数",
           "累计好评率", "累计差评率"]
    train_val = allData[:x].copy()  # .astype('float32')
    test = allData[x:y].copy()
    label_x = train_val['销量']
    test_val = allData[x:y].copy()[col]
    x_train_val = xgb.DMatrix(train_val[col], label=label_x)
    test_val = xgb.DMatrix(test_val[col])
    # params = {
    #     # 'booster': 'gbtree',
    #     # 这里手写数字是0-9，是一个多类的问题，因此采用了multisoft多分类器，
    #     # 'objective': 'reg:linear',
    #     # 'num_class': 10,  # 类数，与 multisoftmax 并用
    #     # 'gamma': 0.05,  # 在树的叶子节点下一个分区的最小损失，越大算法模型越保守 。[0:]
    #     'max_depth': 5,  # 构建树的深度 [1:]
    #     # 'lambda':450,  # L2 正则项权重
    #     # 'subsample': 0.4,  # 采样训练数据，设置为0.5，随机选择一般的数据实例 (0:1]
    #     # 'colsample_bytree': 0.7,  # 构建树树时的采样比率 (0:1]
    #     # 'min_child_weight':12, # 节点的最少特征数
    #     # 'silent': 1,
    #     'eta': 0.005,  # 如同学习率
    #     'seed': 710,
    #     'nthread': 4,  # cpu 线程数,根据自己U的个数适当调整
    # }
    # plst = params.items()

    param = {'max_depth': 5, 'eta': 0.1, 'nthread': 4}
    # param['objective'] = 'reg:gamma'
    model = xgb.train(param, x_train_val,num_boost_round=50)
    preds = model.predict(test_val)
    # test['销量'] = preds

    rmse = 'RMSE: %.4f' % np.sqrt(sum((preds - test['销量'].values) ** 2) / test['销量'].size)
    mape = sum(abs(test['销量'].values - preds)) * 100 / (len(test['销量']) * sum(test['销量']))
    i = i + 1
    print('MAPE', mape, rmse)
