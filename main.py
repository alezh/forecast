# 评价数据
from random import randint, uniform

import shap
from sklearn import metrics

from entity.Evaluate import Evaluate
from entity.PageViews import PageViews
from entity.Sales import Sales
from utils.pgsql import CONN
import pandas as pd
import xgboost
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split, RandomizedSearchCV, StratifiedKFold

plt.style.use('seaborn')


def pgInit():
    # 评价
    Evaluate().evaCsvInsert()
    # 销量
    Sales().salesCsvInsert()
    # 流量
    PageViews().pageViewsCsvInsert()
    cols = ['hao', 'zhong', 'cha', 'hf', 'avgH', 'avgZ', 'avgC', 'rateH', 'rateZ', 'rateC', 'avgPrice', 'price', 'hd',
            'pv', 'uv', 'cv', 'jsv', 'avgt', 'sv']
    eva = pd.read_csv('cache/eva.csv')
    sales = pd.read_csv('cache/sales.csv')
    pageViews = pd.read_csv('cache/pageViews.csv')
    # 填充null
    # result = pd.merge(eva, sales, how='outer', on=['week', 'year'])
    mg = pd.merge(eva, sales, on=['week', 'year'])
    result = pd.merge(mg, pageViews, on=['week', 'year'])
    train = result[5:130].copy()
    test = result[130:-10].copy()
    y_test_org = test.copy()
    test['num'] = 0
    # loadDataSet(train, train['num'], cols)
    # model = xgbost(loadDataSet(train, train['num'], cols), train, train['num'], cols)
    # 特征重要性
    model = xgb(train[cols], train['num'].values)
    predict(model, test, cols)
    # feature(model, result, cols)
    # pltShow(model, cols)
    print('MAPE:', sum(abs((test['num'] - y_test_org['num']) / y_test_org['num'])) / y_test_org['num'].size * 100)
    # score = model.score(y_test_org[cols], test['num'])
    # print("R^2 (Train): %f" % score)
    linePlt(y_test_org, test, 'num')


def trains(train, cols, test):
    y_test_org = test.copy()
    # test['件数'] = 0
    model = xgb(train[cols], train['num'].values)
    predict(model, test, cols)

    print('MAPE:', sum(abs((test['num'] - y_test_org['num']) / y_test_org['num'])) / y_test_org['num'].size * 100)
    linePlt(y_test_org, test, 'num')


def xgb(x, y):
    # model = xgboost.XGBRegressor(max_depth=5, learning_rate=0.1, n_estimators=160, silent=False,
    #                              objective='reg:gamma')
    model = xgboost.XGBRegressor(max_depth=5, learning_rate=0.1, n_estimators=160)
    model.fit(x, y)

    return model


def predict(model, result, cols):
    ans = model.predict(result[cols])
    result['num'] = ans


def pltShow(model, cols):
    plt.figure(figsize=(15, 5))
    plt.bar(range(len(cols)), model.feature_importances_)
    plt.xticks(range(len(cols)), cols, rotation=-45, fontsize=14)
    plt.title('Feature importance', fontsize=14)
    plt.show()


def linePlt(result, test, y_key):
    test_true_data = pd.DataFrame(data={'date': result['week'], 'actual': result[y_key].values})
    test_pred_data = pd.DataFrame(data={'date': test.week, 'prediction': test[y_key].values})
    plt.figure()
    test_pred_data.prediction.plot(color='red', label='Predict', figsize=(12, 8))
    test_true_data.actual.plot(color='blue', label='Original', figsize=(12, 8))
    plt.title('RMSE: %.4f' % np.sqrt(sum((test[y_key] - result[y_key]) ** 2) / result[y_key].size))
    plt.legend(loc='best')  # 将样例显示出来
    plt.grid(True)
    plt.show()


def loadDataSet(traindata, y, cols):
    seed = 42
    # 处理非数值型数据
    params_dist_grid = {
        'max_depth': [int(x) for x in np.linspace(10, 100, num=10)],
        'gamma': [0, 0.5, 1],
        'n_estimators': [int(x) for x in np.linspace(start=200, stop=2000, num=10)],
        'learning_rate': [0.1],  # gaussian distribution
        'subsample': [0.5, 0.6, 1],  # gaussian distribution
        'colsample_bytree': [0.5, 0.6, 1]  # gaussian distribution
    }
    params_fixed = {
        'objective': 'binary:logistic',
        'silent': 1
    }
    # 与 GridSearchCV 类似
    rs_grid = RandomizedSearchCV(
        estimator=xgboost.XGBClassifier(**params_fixed, seed=seed),
        param_distributions=params_dist_grid,
        n_iter=10,
        cv=2,
        scoring='accuracy',
        random_state=seed
    )
    rs_grid.fit(traindata[cols], y)
    return rs_grid.best_params_


def xgbost(params, x, y, cols):
    model = xgboost.XGBClassifier(**params, seed=42)
    model.fit(x[cols], y)
    return model


def feature(model, result, cols):
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(result[cols])
    print(shap_values.shape)
    y_base = explainer.expected_value
    print(y_base)
    result['pred'] = model.predict(result[cols])
    print(result['pred'].mean())
    j = 30
    shap.initjs()
    shap.force_plot(explainer.expected_value, shap_values[j], result[cols].iloc[j])


def Data():
    # 获取训练数据
    eva = Evaluate().train_pd()
    sales = Sales().train_pd()
    pageView = PageViews().train_pd()
    # 合并训练数据
    me = pd.merge(sales, eva, on=['week', 'year'])
    mes = pd.merge(me, pageView, on=['week', 'year'])
    train = mes.copy()

    name = ['WeeksAgoNum', 'twoWeeksAgoNum', 'threeWeeksAgoNum']
    for i, v in enumerate(name):
        # 销售数据 合并 一周前 二州前 三周前 数据
        train[v] = mes.iloc[i + 1:, 2].copy().reset_index(drop=True)
    train['pv'] = mes.iloc[1:, 13].copy().reset_index(drop=True)
    train['uv'] = mes.iloc[1:, 14].copy().reset_index(drop=True)
    train['sc'] = mes.iloc[1:, 15].copy().reset_index(drop=True)
    train['jg'] = mes.iloc[1:, 16].copy().reset_index(drop=True)
    # uv pv 一周前 二州前 三周前 数据
    cols = ['avgprice', 'avgpd',  # 平均单价 平均优惠
            'hao', 'p', 'c', 'h', 'ahao', 'ap', 'ac',  # 平均
            'pv', 'uv', 'sc', 'jg', 'avgst', 'se',
            'WeeksAgoNum', 'twoWeeksAgoNum']
    # train.columns.values
    train.to_csv('cache/train1.csv',index=False)
    x_train = train[:131].copy().reset_index(drop=True)
    y_test = train[131:-9].copy().reset_index(drop=True)
    trains(x_train, cols, y_test)


if __name__ == '__main__':
    Data()
    CONN.close()
