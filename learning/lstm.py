import pandas as pd
import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM
from skimage.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from torch.utils.data import Dataset, DataLoader
from torch import nn
import torch

from net.models import LSTMNet


def scale(train_features, test_features):
    scaler = MinMaxScaler()
    data = pd.concat([train_features, test_features])
    scaled_data = pd.DataFrame(scaler.fit_transform(data), columns=test_features.keys())
    train_features = scaled_data.iloc[0:train_features.shape[0]]
    test_features = scaled_data.iloc[train_features.shape[0]:]
    return train_features, test_features


def LSTM_Regression(train_features, train_y, test_x, test_y):
    X_train, X_test = scale(train_features, test_x)

    X_train = X_train.values.reshape(X_train.shape[0], 1, X_train.shape[1])
    X_test = X_test.values.reshape(X_test.shape[0], 1, X_test.shape[1])

    X_train, X_val, y_train, y_val = train_test_split(X_train, train_y, test_size=0.2, random_state=5)
    model = Sequential()
    model.add(LSTM(52, activation='tanh', input_shape=(X_train.shape[1], X_train.shape[2])))
    model.add(Dense(1, activation='relu'))
    model.compile(loss='mse', optimizer='adam')
    model.fit(X_train, y_train, epochs=1100, batch_size=100, validation_data=(X_val, y_val), verbose=2, shuffle=False)
    # model = load_model('lstm_model_8805_a8485.h5')
    y_val_pred = model.predict(X_val)
    y_test_pred = model.predict(X_test)
    # rmse = mean_squared_error(y_val, y_val_pred) ** 0.5
    # score = 1.0 / (1.0 + rmse)
    y_test_pred = np.array(y_test_pred).flatten()
    pass


class TrainSet(Dataset):
    def __init__(self, train, train_y):
        # 定义好 image 的路径
        self.data, self.label = train.float(), train_y.float()

    def __getitem__(self, index):
        return self.data[index], self.label[index]

    def __len__(self):
        return len(self.data)


def LSTM_Torch(train_features, train_y, test_x, test_y):
    train_features = train_features.apply(lambda x: (x - min(x)) / (max(x) - min(x)))
    train_features = train_features.drop(columns='1')
    test_x = test_x.drop(columns='1')
    # 运行环境
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    train_x = torch.tensor(train_features.values, device=device, dtype=torch.float32)
    train_y = torch.tensor(train_y.values, device=device, dtype=torch.float32)
    trainset = TrainSet(train_x, train_y)

    n = train_features.shape[1]  # n为模型中的n
    LR = 0.001  # LR是模型的学习率
    EPOCH = 100  # EPOCH是多次循环
    batch_size = 10

    trainloader = DataLoader(trainset, batch_size=batch_size, shuffle=False)

    net = LSTMNet(n, 52).cuda()
    criterion = nn.MSELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=LR)
    for e in range(EPOCH):
        for i, (X, y) in enumerate(trainloader):
            var_x = X.view(-1, 1, X.shape[1])
            # var_y = y
            out = net(var_x)
            loss = criterion(out, y)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            print('Epoch: {}, Loss: {:.5f}'.format(e + 1, loss.item()))
    test_x = torch.tensor(test_x.values, device=device, dtype=torch.float32)
    pres = net(test_x.view(-1, 1, test_x.shape[1]))
    pass
