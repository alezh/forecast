import gc

import numpy as np
import pandas as pd
from lightgbm import LGBMRegressor
from sklearn.model_selection import KFold


def l_model(train_x, train_y, test_x, test_y, n_folds=4):
    feature_importance_values = np.zeros(train_x.shape[1])
    test_predictions = np.zeros(test_x.shape[0])
    train_predictions = np.zeros(train_x.shape[0])
    out_of_fold = np.zeros(train_x.shape[0])
    feature_names = list(train_x.columns)
    k_fold = KFold(n_splits=n_folds, shuffle=True, random_state=10)
    train_scores = []
    valid_scores = []
    cat_indices = 'auto'
    for train_indices, valid_indices in k_fold.split(train_x):
        # 训练数据
        train_features, train_labels = train_x.values[train_indices], train_y.values[train_indices]
        # 验证集
        valid_features, valid_labels = train_x.values[valid_indices], train_y.values[valid_indices]

        model = LGBMRegressor(objective='regression', n_estimators=16000, min_child_samples=20, num_leaves=31,
                              learning_rate=0.005, feature_fraction=0.8,
                              subsample=0.5, n_jobs=4, random_state=50)
        model.fit(train_features, train_labels, eval_metric='rmse',
                  eval_set=[(valid_features, valid_labels), (train_features, train_labels)],
                  eval_names=['valid', 'train'], categorical_feature=cat_indices,
                  early_stopping_rounds=2000, verbose=200)
        best_iteration = model.best_iteration_
        feature_importance_values += model.feature_importances_ / k_fold.n_splits
        test_predictions += model.predict(test_x, num_iteration=best_iteration) / k_fold.n_splits
        train_predictions += model.predict(train_x, num_iteration=best_iteration) / k_fold.n_splits
        out_of_fold[valid_indices] = model.predict(valid_features, num_iteration=best_iteration) / k_fold.n_splits
        valid_score = model.best_score_['valid']['rmse']
        train_score = model.best_score_['train']['rmse']
        valid_scores.append(valid_score)
        train_scores.append(train_score)
        gc.enable()
        del model, train_features, valid_features
        gc.collect()
    submission = pd.DataFrame({'num': test_predictions})
    train_sub = pd.DataFrame({'num': train_predictions})
    feature_importances = pd.DataFrame({'feature': feature_names, 'importance': feature_importance_values})
    train_scores.append(np.mean(train_scores))
    valid_scores.append(np.mean(valid_scores))
    fold_names = list(range(n_folds))
    fold_names.append('overall')
    metric = pd.DataFrame({'fold': fold_names,
                           'train': train_scores,
                           'valid': valid_scores})
    return submission, feature_importances, metric, train_sub
