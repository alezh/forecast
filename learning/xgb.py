import gc
import pandas as pd
import xgboost as xgb
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold


def x_model(train_x, train_y, test_x, test_y, n_folds=4):
    feature_importance_values = np.zeros(train_x.shape[1])
    test_predictions = np.zeros(test_x.shape[0])
    train_predictions = np.zeros(train_x.shape[0])
    train_scores = []
    feature_names = list(train_x.columns)
    k_fold = KFold(n_splits=n_folds, shuffle=True, random_state=10)
    for train_indices, valid_indices in k_fold.split(train_x):
        # 训练数据
        train_features, train_labels = train_x.values[train_indices], train_y.values[train_indices]
        # 验证集
        valid_features, valid_labels = train_x.values[valid_indices], train_y.values[valid_indices]

        model = xgb.XGBRegressor(objective='reg:linear', n_estimators=16000, min_child_weight=1, num_leaves=20,
                                 learning_rate=0.1, max_depth=6, n_jobs=4,
                                 subsample=0.6, colsample_bytree=0.4, colsample_bylevel=1)
        model.fit(train_features, train_labels,
                  eval_set=[(valid_features, valid_labels), (train_features, train_labels)],
                  early_stopping_rounds=300, verbose=600)
        # 记录特征值
        feature_importance_values += model.feature_importances_ / k_fold.n_splits

        test_predictions += model.predict(test_x.values) / k_fold.n_splits
        train_predictions += model.predict(train_x.values) / k_fold.n_splits

        out_of_fold = model.predict(valid_features) / k_fold.n_splits
        train_score = my_scorer(valid_labels, out_of_fold, valid_features)

        train_scores.append(train_score)

        # Clean up memory
        gc.enable()
        del model, train_features, valid_features
        gc.collect()
    submission = pd.DataFrame({'num': test_predictions})
    train_sub = pd.DataFrame({'num': train_predictions})
    feature_importances = pd.DataFrame({'feature': feature_names, 'importance': feature_importance_values})
    train_scores.append(np.mean(train_scores))
    fold_names = list(range(n_folds))
    fold_names.append('overall')
    metric = pd.DataFrame({'fold': fold_names,
                           'train': train_scores,
                           })
    return submission, feature_importances, metric, train_sub


def my_scorer(y_true, y_predicted, X_test):
    loss_train = np.sum((y_true - y_predicted) ** 2, axis=0) / (X_test.shape[0])  # RMSE
    loss_train = loss_train ** 0.5
    score = 1 / (1 + loss_train)
    return score
