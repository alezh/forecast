import pandas as pd
from sklearn.model_selection import train_test_split as t_t_s
from sklearn.model_selection import KFold
from sklearn.preprocessing import PolynomialFeatures


class Processing(object):

    def __init__(self):
        pass

    # Transform the features
    def features(self, train_x, test_x, cols, degree=2, y_target=None):
        poly_transformer = PolynomialFeatures(degree=degree)
        poly_transformer.fit(train_x)
        poly_features = poly_transformer.transform(train_x)
        poly_features_test = poly_transformer.transform(test_x)
        poly_features = pd.DataFrame(poly_features, columns=poly_transformer.get_feature_names(cols))
        if y_target is not None:
            self._correlations(poly_features, y_target)
        poly_features_test = pd.DataFrame(poly_features_test, columns=poly_transformer.get_feature_names(cols))
        return poly_features, poly_features_test

    def _correlations(self, poly_features, y_target):
        # Add in the target
        poly_features['TARGET'] = y_target
        # Find the correlations with the target
        poly_corrs = poly_features.corr()['TARGET'].sort_values()
        print(poly_corrs)
