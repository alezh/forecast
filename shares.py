import datetime

import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from torch import nn
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader

from utils.Read import Read

maximums, minimums, avgs = [], [], []
cols = ['收盘价', '最高价', '最低价', '开盘价', '前收盘', '涨跌额', '涨跌幅', '成交量', '成交金额']

bond = Read("cache/daily.csv")
# plt.figure()
# bond.开盘价.plot(color='blue', label='Original', figsize=(12, 8))
# plt.legend(loc='best')  # 将样例显示出来
# plt.grid(True)
# plt.show()
