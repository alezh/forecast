import pandas as pd
import numpy as np
import xgboost
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from xgboost import plot_importance

from entity.Evaluate import Evaluate
from entity.PageViews import PageViews
from entity.Sales import Sales
from utils.pgsql import CONN

# from xgboost.sklearn import XGBClassifier
plt.style.use('seaborn')


# 数据合并
def GetData():
    eva = Evaluate().daily_pd()
    sales = Sales().daily_pd()
    pageView = PageViews().daily_pd()
    merge = pd.merge(sales, eva, on=['days', 'day', 'week', 'year'])
    train = pd.merge(merge, pageView, on=['days', 'day', 'week', 'year']).reset_index(drop=True)
    train = train.query("day != '11-11' and day != '12-12'").reset_index(drop=True)
    name = ['WeeksAgoNum', 'twoWeeksAgoNum', 'threeWeeksAgoNum']
    for i, v in enumerate(name):
        # 销售数据 合并 一周前 二州前 三周前 数据
        train[v] = train['num'].shift(i + 1)
    train['pv'] = train['pv'].shift(1)
    train['uv'] = train['uv'].shift(1)
    train['sc'] = train['sc'].shift(1)
    train['jg'] = train['jg'].shift(1)
    train['at'] = train['at'].shift(1)
    train['hao'] = train['hao'].shift(1)
    train['ping'] = train['ping'].shift(1)
    train['cha'] = train['cha'].shift(1)
    train['hf'] = train['hf'].shift(1)
    train.to_csv("cache/daily.csv", index_label="i")
    data_cut(train)


def data_cut(train):
    col = ['discount', 'hd', 'hao', 'ping', 'cha', 'hf', 'avghao', 'avgping', 'avgcha', 'avghf', 'pv', 'uv',
           'sc', 'jg', 'at', 'sh', 'WeeksAgoNum', 'twoWeeksAgoNum']
    # X_train, X_test, y_train, y_test = train_test_split(train[col], train['num'], test_size=0.8, random_state=0)
    # j = len(train)
    # i = 1
    # while (2 * i + 6) <= j:
    #     x = 2 * i + 2
    #     y = 2 * i + 4
    #     train_x = train[:x]
    #     label = train[:x]['num']
    #     test_x = train[x:y].copy()
    #     xgb(train_x, label, test_x, col)
    #     i = i + 1
    train_len = int(len(train) * 0.8)
    train_x = train[:train_len]
    label = train[:train_len]['num']
    test = train[train_len:].copy()
    xgb(train_x, label, test, col)


def xgb_train(train, label_x, test, col):
    org_x = test.copy()
    train_x = xgboost.DMatrix(train[col], label=label_x)
    test_x = xgboost.DMatrix(test[col])
    param = {'max_depth': 5, 'gamma': 0.1, 'eta': 0.1, 'nthread': 4, 'seed': 710}
    model = xgboost.train(param, train_x, num_boost_round=10)
    preds = model.predict(test_x)
    test['num'] = preds
    print('MAPE:', sum(abs((preds - org_x['num']) / org_x['num'])) / org_x['num'].size * 100)
    # 计算准确率

    # accuracy = accuracy_score(org_x['num'], preds)
    # print('accuarcy:%.2f%%' % (accuracy * 100))
    # plt.clf()
    # plot_importance(model)
    # plt.show()
    # pltShow(model,col)
    linePlt(org_x, test, 'num')


def xgb(train, label_y, test, cols):
    org_x = test.copy()
    # model = xgboost.XGBRegressor(max_depth=5, learning_rate=0.01, n_estimators=100,nthread=4, silent=True)
    # model = xgboost.XGBClassifier(max_depth=5,learning_rate=0.1,n_estimators=160,
    #                               reg_lambda=2,
    #                               nthread=4,silent=True,objective='multi:softmax')
    model = xgboost.XGBRegressor(max_depth=5, learning_rate=0.1, n_estimators=100, nthread=4, silent=True,
                                 objective='reg:gamma')
    model.fit(train[cols], label_y)
    result = model.predict(test[cols])
    # test['num'] = result
    print('MAPE:', sum(abs((result - org_x['num']) / org_x['num'])) / org_x['num'].size * 100)
    linePlt(org_x, result, 'num')
    pltShow(model, cols)
    return result


def pltShow(model, cols):
    plt.clf()
    plt.figure(figsize=(15, 5))
    plt.bar(range(len(cols)), model.feature_importances_)
    plt.xticks(range(len(cols)), cols, rotation=-45, fontsize=14)
    plt.title('Feature importance', fontsize=14)
    plt.show()


def linePlt(result, test, y_key):
    test_true_data = pd.DataFrame(data={'actual': result[y_key].values})
    test_pred_data = pd.DataFrame(data={'prediction': test})
    plt.figure()
    test_pred_data.prediction.plot(color='red', label='Predict', figsize=(20, 8))
    test_true_data.actual.plot(color='blue', label='Original', figsize=(20, 8))
    # plt.title('RMSE: %.4f' % np.sqrt(sum((test[y_key] - result[y_key]) ** 2) / result[y_key].size))
    plt.legend(loc='best')  # 将样例显示出来
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    GetData()
    CONN.close()
