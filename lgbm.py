import lightgbm as lgb
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

col = ['discount', 'hd', 'hao', 'ping', 'cha', 'hf', 'avghao', 'avgping', 'avgcha', 'avghf', 'pv', 'uv',
       'sc', 'jg', 'at', 'sh', 'WeeksAgoNum', 'twoWeeksAgoNum']
ALL_DATA = pd.read_csv('cache/daily.csv')
train = ALL_DATA.query("day != '11-11'").reset_index(drop=True)
train_len = int(len(train) * 0.8)
train_x = train[:train_len][col]
label = train[:train_len]['num']
test = train[train_len:][col].copy()
label_t = train[train_len:]['num']
lgb_train = lgb.Dataset(train_x, label)  # 将数据保存到LightGBM二进制文件将使加载更快
lgb_eval = lgb.Dataset(test, label_t, reference=lgb_train)  # 创建验证数据
# lgb.cv()
gbm = lgb.LGBMRegressor(objective='regression', num_leaves=2 ** 5, learning_rate=0.05, n_estimators=20)
gbm.fit(train_x, label, eval_set=[(test, label_t)], eval_metric='l1', early_stopping_rounds=5)

# params = {
#     'task': 'train',
#     'boosting_type': 'gbdt',  # 设置提升类型
#     'objective': 'regression',  # 目标函数
#     'metric': {'l2', 'auc'},  # 评估函数
#     'num_leaves': 31,  # 叶子节点数
#     'learning_rate': 0.1,  # 学习速率
#     'feature_fraction': 0.9,  # 建树的特征选择比例
#     'bagging_fraction': 0.8,  # 建树的样本采样比例
#     'bagging_freq': 5,  # k 意味着每 k 次迭代执行bagging
#     'verbose': 1  # <0 显示致命的, =0 显示错误 (警告), >0 显示信息
# }
# gbm = lgb.train(params, lgb_train, num_boost_round=20, valid_sets=lgb_eval, early_stopping_rounds=5)
# # 预测数据集
y_pred = gbm.predict(test, num_iteration=gbm.best_iteration_)

print('MAPE:', sum(abs((y_pred - label_t) / label_t)) / label_t.size * 100)

plt.clf()
plt.figure(figsize=(15, 5))
plt.bar(range(len(col)), gbm.feature_importances_)
plt.xticks(range(len(col)), col, rotation=-45, fontsize=14)
plt.title('Feature importance', fontsize=14)
plt.show()

test_true_data = pd.DataFrame(data={'actual': label_t.values})
test_pred_data = pd.DataFrame(data={'prediction': y_pred})
plt.figure()
test_pred_data.prediction.plot(color='red', label='Predict', figsize=(12, 8))
test_true_data.actual.plot(color='blue', label='Original', figsize=(12, 8))
# plt.title('RMSE: %.4f' % np.sqrt(sum((test[y_key] - result[y_key]) ** 2) / result[y_key].size))
plt.legend(loc='best')  # 将样例显示出来
plt.grid(True)
plt.show()

# 网格搜索，参数优化
estimator = lgb.LGBMRegressor(num_leaves=2 ** 5)
param_grid = {
    'learning_rate': [0.05, 0.01, 0.1, 1],
    'n_estimators': [20, 40, 50, 150]
}
gbm = GridSearchCV(estimator, param_grid)
gbm.fit(train_x, label)
print('Best parameters found by grid search are:', gbm.best_params_)
