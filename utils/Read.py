import datetime

import pandas as pd

from entity.Evaluate import Evaluate
from entity.PageViews import PageViews
from entity.Sales import Sales


def train_test_split(train, train_len, col):
    t_len = int(len(train) * train_len)
    train_x = train[:t_len]
    train_y = train[:t_len][col]
    test_x = train[t_len:].copy()
    test_y = test_x[col]
    return train_x, train_y, test_x, test_y


class ReadX(object):

    def __init__(self, path):
        self.database = pd.read_csv(path).sort_index(axis=0, ascending=False).fillna(0)

    database = pd.DataFrame({})

    def normalized(self, bonds):
        maximums, minimums, avgs = bonds.max(axis=0), bonds.min(axis=0), bonds.sum(axis=0) / bonds.shape[0]
        for i, v in enumerate(bonds.columns):
            bonds[v] = (bonds[v] - minimums[v]) / (maximums[v] - minimums[v])
        # y = bonds.apply(lambda x: (x - min(x)) / (max(x) - min(x)))
        return bonds

    def search(self, name, query="日期 >= '2005-01-01'"):
        self.database = self.database.query(query)[name].astype("float64")

    def time_to_index(self, field):
        self.database.index = list(map(lambda x: datetime.datetime.strptime(x, "%Y/%m/%d"), self.database.日期))


class PgSqlToCsv(object):
    cols = ['week', 'year', 'month', 'total', '优惠金额', '折扣', '原价', 'spu_tm', 'hao',
            'p', 'c', 'h', 'ahao', 'ap', 'ac', 'pv', 'uv', 'sc', 'jg', 'avgst',
            'se']

    All_SPU = []

    def __init__(self):
        pass

    def week_spu_data(self):
        eva = Evaluate().train_pd().fillna(0)
        sales = Sales().week_spu_data().fillna(0)
        pageView = PageViews().train_pd().fillna(0)
        sales = pd.merge(sales, eva, on=['week', 'year', 'spu_tm'])
        sales = pd.merge(sales, pageView, on=['week', 'year', 'spu_tm'])
        return sales

    def week(self, sales):
        org = pd.DataFrame(columns=[])
        name = ['WeeksAgoNum', 'twoWeeksAgoNum', 'threeWeeksAgoNum']
        for v, p in sales.groupby('spu_tm'):
            o = p.copy().reset_index(drop=True)
            o['pv'] = o.loc[1:, ['pv']].copy().reset_index(drop=True)
            o['uv'] = o.loc[1:, ['uv']].copy().reset_index(drop=True)
            o['sc'] = o.loc[1:, ['sc']].copy().reset_index(drop=True)
            o['jg'] = o.loc[1:, ['jg']].copy().reset_index(drop=True)
            o['avgst'] = o.loc[1:, ['avgst']].copy().reset_index(drop=True)
            o['hao'] = o.loc[1:, ['hao']].copy().reset_index(drop=True)
            o['se'] = o.loc[1:, ['se']].copy().reset_index(drop=True)
            o['p'] = o.loc[1:, ['p']].copy().reset_index(drop=True)
            o['c'] = o.loc[1:, ['c']].copy().reset_index(drop=True)
            o['h'] = o.loc[1:, ['h']].copy().reset_index(drop=True)
            o['ap'] = o.loc[1:, ['ap']].copy().reset_index(drop=True)
            o['ac'] = o.loc[1:, ['ac']].copy().reset_index(drop=True)
            o['ahao'] = o.loc[1:, ['ahao']].copy().reset_index(drop=True)
            for i, x in enumerate(name):
                # 销售数据 合并 一周前 二州前 三周前 数据
                o[x] = o.loc[i + 1:, ['total']].copy().reset_index(drop=True)
            o.to_csv('cache/train/' + v + '.csv', index=False)
            if org.size == 0:
                org = o
            else:
                org = pd.concat([org, o])
            self.All_SPU.append(v)
        org.to_csv('cache/week.csv', index=False)
        return org

    def sales_sku_data(self):
        sales = Sales().week_sku_data().fillna(0)
        return sales
