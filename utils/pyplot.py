import matplotlib.pyplot as plt
import pandas as pd

plt.style.use('seaborn')


def pltShow(model, cols):
    plt.figure(figsize=(15, 5))
    plt.bar(range(len(cols)), model)
    plt.xticks(range(len(cols)), cols, rotation=-45, fontsize=14)
    plt.title('Feature importance', fontsize=14)
    plt.show()


def linePlt(result, pred):
    true_data = pd.DataFrame(data={'actual': result})
    pred_data = pd.DataFrame(data={'prediction': pred})
    plt.figure()
    pred_data.prediction.plot(color='red', label='Predict', figsize=(12, 8))
    true_data.actual.plot(color='blue', label='Original', figsize=(12, 8))
    # plt.title('RMSE: %.4f' % np.sqrt(sum((test[y_key] - result[y_key]) ** 2) / result[y_key].size))
    plt.legend(loc='best')  # 将样例显示出来
    plt.grid(True)
    plt.show()


def line(result):
    true_data = pd.DataFrame(data={'actual': result})
    plt.figure()
    true_data.plot(color='red', label='Predict', figsize=(12, 8))
    plt.legend(loc='best')  # 将样例显示出来
    plt.grid(True)
    plt.show()
