SELECT week,year,sum(hao),sum(ping),sum(cha),sum(hf),count(*),AVG(hao),AVG(ping),avg(cha) FROM (SELECT EXTRACT
	( week FROM 评论时间 ) :: INTEGER AS week,
	EXTRACT ( YEAR FROM 评论时间 ) :: INTEGER AS year,
	CASE WHEN 评论得分>0 THEN 1 ELSE 0 END AS hao,
	CASE WHEN 评论得分=0 THEN 1 ELSE 0 END AS ping,
	CASE WHEN 评论得分<0 THEN 1 ELSE 0 END AS cha,
	CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END AS hf
FROM 销售预测_评论数据 WHERE spu_tm = 'AA000006' ORDER BY year) as pj
GROUP BY week,year
ORDER BY week,year