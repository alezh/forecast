SELECT pl.week,pl.year,sum(pl.pv) as pv,sum(pl.uv) as uv,sum(pl.收藏数) as 收藏数,sum(pl.加购数) as 加购数,AVG(平均停留时长) as 平均停留时长, sum(搜索引导访客数) as 搜索引导访客数 FROM
(SELECT extract (week from 日期)::integer as week,extract (YEAR from 日期)::integer as year,pv,uv,"收藏数","加购数","平均停留时长","搜索引导访客数" FROM "public"."销售预测_流量数据" WHERE spu_tm ='AA000006') as pl
GROUP BY pl.week,pl.year ORDER BY pl.year,pl.week



SELECT
	week,YEAR,SUM(pv) AS pv,SUM(uv) AS uv,
	SUM (收藏数) AS 收藏数,
	SUM (加购数) AS 加购数,
	CASE WHEN AVG(平均停留时长)>0 THEN AVG(平均停留时长) ELSE 0 END AS 平均停留时长,
	CASE WHEN SUM(搜索引导访客数)>0 THEN SUM(搜索引导访客数) ELSE 0 END AS 搜索引导访客数
FROM
	(
	SELECT EXTRACT
		(week FROM 日期)::INTEGER AS week,
		EXTRACT (YEAR FROM 日期)::INTEGER AS YEAR,
		pv,
		uv,
		收藏数,
		加购数,
		平均停留时长,
		搜索引导访客数
	FROM
		销售预测_流量数据
	WHERE
		spu_tm = 'AA000006'
	) AS pl
GROUP BY week,YEAR ORDER BY YEAR,week


SELECT to_char(日期,'YYYY-MM-DD') AS days,EXTRACT(week FROM 日期)::INTEGER AS week
   ,EXTRACT(YEAR FROM 日期)::INTEGER AS YEAR,SUM(pv) AS pv,SUM(uv) AS uv,SUM (收藏数) AS 收藏数,
	SUM (加购数) AS 加购数,CASE WHEN AVG(平均停留时长)>0 THEN AVG(平均停留时长) ELSE 0 END AS 平均停留时长,
	CASE WHEN SUM(搜索引导访客数)>0 THEN SUM(搜索引导访客数) ELSE 0 END AS 搜索引导访客数
	FROM
		销售预测_流量数据
	WHERE
		spu_tm = 'AA000006'
GROUP BY days,week,YEAR
ORDER BY days