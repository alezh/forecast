SELECT
	week,
	YEAR,
	SUM ( 付款件数 ) as 件数,
	avg(CASE WHEN 常规销售单价-单价>0 THEN 常规销售单价-单价 ELSE 单价 END)::NUMERIC(19,2) as 平均价格,
	avg(CASE WHEN 常规销售单价-单价>0 THEN 1 ELSE 0 END)::NUMERIC(19,2) as 活动次数
FROM
	(
	SELECT EXTRACT
		( week FROM 日期 ) :: INTEGER AS week,
		EXTRACT ( YEAR FROM 日期 ) :: INTEGER AS YEAR,日期,	店铺代码_tm,订单号_tm,
		sku_tm,	付款件数,付款金额,常规销售单价,平台,付款金额/付款件数 as 单价
	FROM
		销售预测_交易支付数据
	WHERE
		订单状态 NOT LIKE'%拆单关闭,已作废,已取消%'
		AND sku_tm LIKE'%AA000006%'
	ORDER BY
		year,
		week,店铺代码_tm
	) AS xl
GROUP BY
	year,
	week



SELECT week,year,month,day,sum(付款件数),avg(单价)::NUMERIC(19,2)as 平均单价,
CASE WHEN avg(优惠金额)>0 THEN avg(优惠金额)::NUMERIC(19,2) ELSE 0 END as 平均优惠
 FROM
(SELECT EXTRACT(week FROM 日期 )::INTEGER AS week,
	EXTRACT(MONTH FROM 日期 )::INTEGER AS month,
	EXTRACT(YEAR FROM 日期 )::INTEGER AS YEAR,
	EXTRACT(DAY FROM 日期 )::INTEGER AS day,
	日期,店铺代码_tm,订单号_tm,
	sku_tm,付款件数,付款金额,常规销售单价,平台,(付款金额/付款件数)::NUMERIC(19,2) AS 单价,
	( 常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))::NUMERIC(19,2) AS 优惠金额
FROM
	销售预测_交易支付数据
WHERE
	订单状态 NOT LIKE '%拆单关闭,已作废,已取消%'
	AND sku_tm LIKE '%AA000006%'
ORDER BY
	日期) as sl
GROUP BY year,month,week,day,单价,优惠金额


SELECT week,year,sum(付款件数)::INTEGER as 件数,avg(单价)::NUMERIC(19,2)as 平均单价,
CASE WHEN avg(优惠金额)>0 THEN avg(优惠金额)::NUMERIC(19,2) ELSE 0 END as 平均优惠
 FROM (SELECT EXTRACT(week FROM 日期 )::INTEGER AS week,
	EXTRACT(MONTH FROM 日期 )::INTEGER AS month,	EXTRACT(YEAR FROM 日期 )::INTEGER AS YEAR,
	日期,店铺代码_tm,订单号_tm,sku_tm,付款件数,付款金额,常规销售单价,平台,(付款金额/付款件数)::NUMERIC(19,2) AS 单价,
	(常规销售单价-(付款金额/付款件数)::NUMERIC(19,2))::NUMERIC(19,2) AS 优惠金额
FROM 销售预测_交易支付数据 WHERE 订单状态 NOT LIKE '%拆单关闭,已作废,已取消%'
	AND sku_tm LIKE '%AA000006%'
ORDER BY 日期) as sl GROUP BY year,week




SELECT to_char(日期,'YYYY-MM-DD') AS days,to_char(日期,'MM-DD') AS DAY,EXTRACT (week FROM 日期):: INTEGER AS week,EXTRACT(YEAR FROM 日期):: INTEGER AS YEAR,
CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS 优惠金额,
avg( CASE WHEN (常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))>0 THEN 1 ELSE 0 END )::NUMERIC(19,2) as hd,
avg(CASE WHEN 常规销售单价-付款金额>0 THEN 常规销售单价-付款金额 ELSE 付款金额 END)::NUMERIC(19,2) as 平均价格,
	sum(付款件数) as 件数, substr(sku_tm, 0, 9) as spu FROM 销售预测_交易支付数据
	where sku_tm LIKE '%AA000006%' AND 订单状态 NOT LIKE'%拆单关闭,已作废,已取消%'
GROUP BY 日期,spu ORDER BY	日期


SELECT
  sa.week,sa.year,sum(sa.件数) as total,sa.省,avg(sa.优惠金额)::NUMERIC(19,2) AS 优惠金额,
	avg(sa.折扣)::NUMERIC(19,2) as 折扣,avg(sa.原价)::NUMERIC(19,2) as 原价,sku_tm,substr(sa.sku_tm, 0, 9) as spu
FROM
	(
	  SELECT
	      to_char( 日期, 'YYYY-MM-DD' ) AS days,to_char( 日期, 'MM-DD' ) AS DAY,EXTRACT ( week FROM 日期 ) :: INTEGER AS week,
	      EXTRACT ( MONTH FROM 日期 ) :: INTEGER AS month,EXTRACT ( YEAR FROM 日期 ) :: INTEGER AS YEAR,省,
	      SUM ( 付款件数 ) AS 件数,	sku_tm,substr(sku_tm, 0, 9) as spu,
	      CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS 优惠金额,
	      AVG ( CASE WHEN ( 常规销售单价- ( 付款金额/付款件数 ) :: NUMERIC ( 19, 2 ) ) > 0 THEN 1 ELSE 0 END ) :: INTEGER AS hd,
	      AVG(((付款金额/付款件数 )/常规销售单价)*10) ::NUMERIC(19,2) as 折扣,
	      avg(常规销售单价)::NUMERIC(19,2) as 原价
    FROM
	    "public"."销售预测_交易支付数据_20200420"
    GROUP BY
	     日期,省,sku_tm
	) AS sa
GROUP BY sa.week,sa.year,sa.month,sa.sku_tm,sa.省 ORDER BY sa.year