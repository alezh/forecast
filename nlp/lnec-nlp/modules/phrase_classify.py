import os,time,db,datetime
from modules import logs
import sys

class CommentPhraseClassify():
    db = db.MyDb.initConn()
    log = logs.MyLogs()
    cursor = db.cursor()


    def ProductClassify(self,id):
        # lstupdtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        # lstupdtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # 以下两种写法的传参格式不同
        # self.cursor.execute(
        #     'update comment_train_phrase a,special_wordlist b '
        #     'set a.taglevel1 = b.taglevel1, a.taglevel2 = b.taglevel2 ,a.lstupdtime = "%s"'
        #     'where a.noun = b.word' %(lstupdtime) )
        if id == '':
            sql="update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2 ,lstupdtime = now() from special_wordlist b where a.noun = b.word and (a.taglevel2 is null or a.taglevel2 = '')"
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2 ,lstupdtime = now() from special_wordlist b where a.adword = b.word and (a.taglevel2 is null or a.taglevel2 = '')"
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2, lstupdtime = now() from special_wordlist b, comment_train_sentiment c where a.comment_train_id = c.comment_train_id and a.id = c.comment_train_phrase_id and c.adjective = b.word and (a.taglevel2 is null or a.taglevel2 = '')"
            self.cursor.execute(sql)
            self.db.commit()

            # n包含质量的：商品/质量
            sql = "update comment_train_phrase set taglevel1 = '商品', taglevel2 = '质量', lstupdtime = now() where noun like '%质量%' and (taglevel2 is null or taglevel2 = '')"
            self.cursor.execute(sql)

            # a为值得的：商品/整体
            sql = "update comment_train_phrase set taglevel1 = '商品', taglevel2 = '整体', lstupdtime = now() where adword = '值得' and (taglevel2 is null or taglevel2 = '')"
            self.cursor.execute(sql)
            self.db.commit()
            # n为空的：商品/整体
            sql = "update comment_train_phrase set taglevel1 = '商品', taglevel2 = '整体', lstupdtime = now() where (noun = '' or noun is null)  and(taglevel2 is null or taglevel2 = '')"
            self.cursor.execute(sql)
        else:
            sql = "update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2 ,lstupdtime = now() from special_wordlist b where a.noun = b.word and a.comment_train_id in (%s) " %(id)
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2 ,lstupdtime = now() from special_wordlist b where a.adword = b.word and (a.taglevel2 is null or a.taglevel2 = '') and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_phrase  a  set taglevel1 = b.taglevel1, taglevel2 = b.taglevel2, lstupdtime = now() from special_wordlist b, comment_train_sentiment c where a.comment_train_id = c.comment_train_id and a.id = c.comment_train_phrase_id and c.adjective = b.word and (a.taglevel2 is null or a.taglevel2 = '') and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()

            # n包含质量的：商品/质量
            sql = "update comment_train_phrase a set taglevel1 = '商品', taglevel2 = '质量', lstupdtime = now() where a.noun like '%质量%' and (taglevel2 is null or taglevel2 = '')"
            self.cursor.execute(sql)

            # a为值得的：商品/整体
            sql = "update comment_train_phrase a set taglevel1 = '商品', taglevel2 = '整体', lstupdtime = now() where adword = '值得' and (taglevel2 is null or taglevel2 = '') and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)

            # n为空的：商品/整体
            sql = "update comment_train_phrase a set taglevel1 = '商品', taglevel2 = '整体', lstupdtime = now() where (noun = '' or noun is null)  and (taglevel2 is null or taglevel2 = '') and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
        self.db.commit()

    def ServiceClassify(self):
        # 速度or解答：回复，回答，答复：需要根据后面的词，快、慢等为速度，否则为解答
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '速度', lstupdtime = now() where (noun like '%回复%' or noun like '%答复%' or noun like '%回答%') and (adword like '%及时%' or adword like '%快%' or adword like '%慢%')"
        self.cursor.execute(sql)
        self.db.commit()
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '解答', lstupdtime = now() where (noun like '%回复%' or noun like '%答复%' or noun like '%回答%')  and (adword not like '%及时%' and adword not like '%快%' and adword not like '%慢%')"
        self.cursor.execute(sql)
        self.db.commit()
        # 若n包含态度，但不是物流或快递态度，则标签为客服/态度
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '态度', lstupdtime = now() where noun like '%态度%' and noun !='物流态度' and noun !='快递态度'"
        self.cursor.execute(sql)
        self.db.commit()

        # n为客服特殊词，全部先置为整体，然后按不同情况逐一处理
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '整体', lstupdtime = now() " \
              "where noun in(select word from special_wordlist where wordtype = 's' ) "
        self.cursor.execute(sql)
        self.db.commit()
        # n为客服特殊词，评论中有推荐，则为客服/推荐
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '推荐', lstupdtime = now() " \
              "where noun in(select word from special_wordlist where wordtype = 's' )" \
              "and comment_train_id in(select id from comment_train where content like '%推荐%')"
        self.cursor.execute(sql)
        self.db.commit()

        # n为客服特殊词，a为 耐心 热情 贴心 热心 是:客服/态度
        sql = "update comment_train_phrase set taglevel1 = '客服', taglevel2 = '态度', lstupdtime = now() " \
              "where noun in(select word from special_wordlist where wordtype = 's' ) and (adword like '%耐心%' or adword like '%热情%' or adword like '%热心%' or adword like '%贴心%')"
        self.cursor.execute(sql)
        self.db.commit()


    def LogisticsClassify(self):
        # n为物流特殊词，全部先置为速度，然后按不同情况逐一处理
        sql = "update comment_train_phrase set taglevel1 = '物流', taglevel2 = '速度', lstupdtime = now() " \
              "where noun in(select word from special_wordlist where wordtype = 'l' ) "
        self.cursor.execute(sql)
        self.db.commit()
        # n中有速度，则为速度
        sql = "update comment_train_phrase set taglevel1 = '物流', taglevel2 = '速度', lstupdtime = now() " \
              "where noun like '%速度%'"
        self.cursor.execute(sql)
        self.db.commit()

        # n为 快递费，或者a里面有贵或便宜的：物流/费用
        sql = "update comment_train_phrase set taglevel1 = '物流', taglevel2 = '费用', lstupdtime = now() " \
              "where noun in(select word from special_wordlist where wordtype = 'l' ) " \
              "and (noun = '快递费' or noun = '物流费' or adword like '%贵%' or adword like '%便宜%')"
        self.cursor.execute(sql)
        self.db.commit()

        # 若n为物流快递态度，则标签为物流态度
        sql = "update comment_train_phrase set taglevel1 = '物流', taglevel2 = '态度', lstupdtime = now() where noun ='物流态度' or noun ='快递态度'"
        self.cursor.execute(sql)
        self.db.commit()

    # 态度：若前n=5个词是物流的，则是物流；否则：默认为服务

    def SpeciaWordClassify(self,word):
        if word == '态度':
            self.cursor.execute(
                "select id,comment_train_id,charactertype,nounid,noun,adwordid,adword from comment_train_phrase where  noun = '%s'  order by comment_train_id" % (word))
            rslt = self.cursor.fetchall()
            for r in rslt:
                # print(r[1])
                endid = r[3]
                # 判断“态度”前面是否有物流词,若有，则是物流/态度，否则是客服/态度
                self.cursor.execute("select id,comment_train_id,word from comment_train_wordlist where comment_train_id = '%s' order by id"  % (r[1]))
                rslt2 = self.cursor.fetchall()
                startid = rslt2[0][0]
                # 往前的偏移量3
                dlt = min(endid - startid,3)
                i = 0
                for r2 in rslt2:
                    if r2[0] == endid:
                        lstupdtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                        sql = "update comment_train_phrase  set taglevel1 = %s ,taglevel2 = %s, lstupdtime = %s where id =%s "
                        self.cursor.execute(sql, ("客服", "态度", lstupdtime, r[0]))
                        self.db.commit()
                        for j in range(i,i-dlt,-1):
                            self.cursor.execute("select * from special_wordlist where  "
                                                "wordtype = 'l' and word = '%s'" % (rslt2[j-1][2]))
                            rows = self.cursor.rowcount
                            if rows == 1:
                                sql = "update comment_train_phrase  set taglevel1 = %s ,taglevel2 = %s, lstupdtime = %s where id =%s"
                                self.cursor.execute(sql,('物流','态度',lstupdtime, r[0]))
                                self.db.commit()
                                break

                    i = i + 1

    def close_db(self):
        self.cursor.close()
        self.db.close()

if __name__ == "__main__":  # 主程序
    if len(sys.argv) == 1:
        v_id = ''
    else:
        v_id = "'" + str(sys.argv[1]).replace(',', '\',\'').replace('(', '').replace(')', '') + "'"

    o = CommentPhraseClassify()
    o.ProductClassify(v_id)
    if v_id == '':
        o.ServiceClassify()
        o.LogisticsClassify()
        o.SpeciaWordClassify('态度')
    o.close_db()
