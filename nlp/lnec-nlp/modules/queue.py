import sys
sys.path.append("d:/lnec-nlp")
import os,time,db,datetime
import jieba
import datetime
jieba.load_userdict(os.path.dirname(os.path.dirname(__file__))+"/dicts/userdict.txt")
from modules import logs
from modules import comment_train
from modules import comment_train_phrase
from modules import comment_train_sentiment
from modules import phrase_classify


class QueueProc():
    db = db.MyDb.initConn()
    log = logs.MyLogs()
    cursor = db.cursor()

    def proc_phrase_wordlist(self):
        sql = "select word || weight || charactertype " \
              "from " \
              "(select word, " \
              "case weight " \
                  "when 1 then \' \' " \
                  "when 10 then \' 10000 \' " \
                  "when 100 then \' 20000 \' "\
               "end as weight, " \
               "charactertype " \
               "from phrase_wordlist " \
               ") a"

        self.cursor.execute(sql)
        rows = self.cursor.fetchall()
        with open(os.path.dirname(os.path.dirname(__file__))+"/dicts/userdict.txt", 'w',encoding='utf-8') as fout:
            for row in rows:
                # print(row[0])
                fout.write('%s\n' % (row[0]))


    def close_db(self):
        self.cursor.close()
        self.db.close()

    def proc_add(self):
        sql = "insert into nlp_queue(comment_id, option_user_id, is_exec_success, create_time) select id, 'sys', 0, now() from comment_train where content1 is null and id not in (select comment_id from nlp_queue)"
        self.cursor.execute(sql)
        self.db.commit()

    def proc_queue(self):
        sql1 = "select count(1) from nlp_queue where is_exec_success = 0"

        self.cursor.execute(sql1)
        rtsl = self.cursor.fetchone()

        while(rtsl[0] != 0):
            print(rtsl[0])
            sql2 = "select array_to_string(array_agg(comment_id), ',') from (select  comment_id  from nlp_queue where is_exec_success = 0 limit 10000) a"
            self.cursor.execute(sql2)
            r = self.cursor.fetchone()

            commentid = r[0]
            # print(commentid)
            o1 = comment_train.CommentProc()
            o1.psg_comment_train(commentid)
            o1.del_comment_train_wordlist(commentid)
            o1.psg_comment_train_wordlist(commentid)

            o2 = comment_train_phrase.CommentPhraseProc()
            o2.del_comment_train_phrase(commentid)
            o2.psg_comment_phrase(commentid)

            o3 = phrase_classify.CommentPhraseClassify()
            o3.ProductClassify(commentid)

            sql3 = "update nlp_queue set is_exec_success = 2, update_time = now() where comment_id in (" + str(commentid) + ')'
            self.cursor.execute(sql3)
            self.db.commit()

            self.cursor.execute(sql1)
            rtsl = self.cursor.fetchone()

        q1 = comment_train.CommentProc()
        q1.del_comment_train_wordlist_2()

        q2 = comment_train_phrase.CommentPhraseProc()
        q2.proc_specilaword_single('速度')
        q2.proc_specilaword_single('回复')
        q2.proc_specilaword_single('回答')
        q2.proc_specilaword_single('答复')
        q2.proc_specilaword_single('态度')
        q2.proc_specilaword_single('值得')
        q2.proc_specilaword_single('质量')

        q3 = phrase_classify.CommentPhraseClassify()
        q3.ServiceClassify()
        q3.LogisticsClassify()
        q3.SpeciaWordClassify('态度')

        sql4 = "select count(1) from nlp_queue where is_exec_success = 2"

        self.cursor.execute(sql4)
        rtsl2 = self.cursor.fetchone()

        while(rtsl2[0] != 0):
            print(rtsl2[0])
            sql5 = "select array_to_string(array_agg(comment_id), ',') from (select  comment_id  from nlp_queue where is_exec_success = 2 limit 10000) a"
            self.cursor.execute(sql5)
            r = self.cursor.fetchone()

            commentid = r[0]
            # print(commentid)
            o4 = comment_train_sentiment.SentimentProc()
            o4.del_comment_train_sentiment(commentid)
            o4.sentiment_phrase(commentid)
            o4.sentiment_phrase_score(commentid)

            sql6 = "update nlp_queue set is_exec_success = 1, done_time = now(), update_time = now() where comment_id in (" + str(commentid) + ')'
            self.cursor.execute(sql6)
            self.db.commit()

            self.cursor.execute(sql4)
            rtsl2 = self.cursor.fetchone()

        sql7 = "delete from nlp_queue where is_exec_success = 1"
        self.cursor.execute(sql7)
        self.db.commit()


if __name__ == "__main__":  # 主程序
    q = QueueProc()
    q.proc_phrase_wordlist()
    q.proc_add()
    q.proc_queue()
    q.close_db()