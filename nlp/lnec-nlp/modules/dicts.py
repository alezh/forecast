import os,db,datetime,sys

class DictProc():

    db = db.MyDb.initConn()
    cursor = db.cursor()

    logisticswords_path = os.path.dirname(os.path.dirname(__file__))+"/dicts/logisticswords.txt"  # 特殊物流词
    productwords_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/productwords.txt"    # 特殊产品词
    servicewords_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/servicewords.txt"    # 特殊服务词
    deletewords_path  = os.path.dirname(os.path.dirname(__file__)) + "/dicts/deletewords.txt"   # 删除特殊词清单
    userdict_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/userdict.txt"
    negativeevaluation_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/negativeevaluation.txt"  # 否定情感词清单
    positiveevaluation_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/positiveevaluation.txt"  # 肯定情感词清单
    negative_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/negative.txt"   # 否定副词清单，如：不，没有
    # adshow_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/adshow.txt" # 形容词，无法组成词组时是否要独立展示
    degree_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/degree.txt"  #
    adjectives_path = os.path.dirname(os.path.dirname(__file__)) + "/dicts/adjectives.txt"  #

    #   加载产品、物流、客服的特殊词清单，并插入到special_wordlist
    def load_manualwords(self,wordlist,wordlisttype):
        result = []
        # 处理物流特殊词
        with open(wordlist, 'r', encoding='UTF-8') as f:
            for line in f:
                result.append(list(line.strip('\n').strip(' ').split(' ')))

        if wordlisttype == 'l' or wordlisttype == 'p' or wordlisttype == 's':
            # 把全删的逻辑去掉，需要改为：不存在则插入，待修改
            # self.cursor.execute("delete from  special_wordlist where src = 'manual' and wordtype = '%s'" % (wordlisttype))
            # self.db.commit()

            for value in result:
                logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                value.append(wordlisttype)
                value.append('manual')
                value.append(logtime)
                self.cursor.execute("delete from  special_wordlist where src <> 'manual' and word = '%s'" % (value[0]))
                self.db.commit()
                self.cursor.execute(
                    "insert into special_wordlist(word,charactertype,wordtype,src,lstupdtime) VALUES (%s,%s,%s,%s,%s)", value)
                self.db.commit()

        if wordlisttype == 'negativeevaluation' or wordlisttype == 'positiveevaluation' or wordlisttype == 'negative':
            for value in result:
                logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                value.append('a')
                value.append('p')
                value.append(wordlisttype)
                value.append(logtime)
                self.cursor.execute("select * from special_wordlist where word = '%s'" % (value[0]))
                rows = self.cursor.rowcount
                if rows == 0:
                    print(value)
                    self.cursor.execute(
                        "insert into special_wordlist(word,charactertype,wordtype,src,lstupdtime) VALUES (%s,%s,%s,%s,%s)",
                        value)
                    self.db.commit()

        if wordlisttype == 'userdict' :
            for r in result:
                value=[]
                value.append(r[0])
                logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                if len(r) == 3:
                    value.append(r[2])
                else:
                    value.append(r[1])
                value.append('p')
                value.append(wordlisttype)
                value.append(logtime)

                # self.cursor.execute(
                #     "delete from  special_wordlist where  word = '%s'" % (value[0]))
                # self.db.commit()

                self.cursor.execute("select * from special_wordlist where word = '%s'" % (value[0]))
                rows = self.cursor.rowcount
                if rows == 0:
                    # print(value)
                    self.cursor.execute(
                        "insert into special_wordlist(word,charactertype,wordtype,src,lstupdtime) VALUES (%s,%s,%s,%s,%s)",
                        value)
                    self.db.commit()
                else:
                    self.cursor.execute(
                        "update special_wordlist set charactertype = '%s',lstupdtime = '%s' where word = '%s' "%(value[1],logtime ,value[0]))
                    self.db.commit()

        if wordlisttype == 'del':
            for value in result:
                # print(value[0])
                self.cursor.execute("delete from  special_wordlist where word = '%s'" % (value[0]))
                self.db.commit()

        # if wordlisttype == 'adshow':
        #     for value in result:
        #         # print(value[0])
        #         self.cursor.execute('update  special_wordlist set adshow = 1 where word = "%s"' % (value[0]))
        #         self.db.commit()

        if wordlisttype == 'degree' :
            self.cursor.execute("delete from  degree_wordlist ")
            self.db.commit()
            for value in result:
                self.cursor.execute(
                    "insert into degree_wordlist(word,wordlevel) VALUES (%s,%s)",
                    value)
                self.db.commit()

                self.cursor.execute("update degree_wordlist set score = 2 where wordlevel = '5' ")
                self.cursor.execute("update degree_wordlist set score = 1.5 where wordlevel = '4' ")
                self.cursor.execute("update degree_wordlist set score = 1 where wordlevel = '3' ")
                self.cursor.execute("update degree_wordlist set score = 0.8 where wordlevel = '2' ")
                self.cursor.execute("update degree_wordlist set score = 0.5 where wordlevel = '1' ")
                self.db.commit()
        if wordlisttype == 'adjectives' :
            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            for value in result:
                self.cursor.execute(
                    "update special_wordlist set wordscore =  %s,wordscoreadjust = %s,lstupdtime = %s where word = %s ",
                    (value[1],value[2],logtime,value[0]))
                self.db.commit()

    def close_db(self):
        self.cursor.close()
        self.db.close()

if __name__ == "__main__":  # 主程序
    o = DictProc()

    # 若有则不插入
    #
    # o.load_manualwords(o.negativeevaluation_path, 'negativeevaluation')
    # o.load_manualwords(o.positiveevaluation_path, 'positiveevaluation')
    #
    #
    # # 先删除后插入。先处理userdict，手工插入的l p s，最后来操作
    o.load_manualwords(o.userdict_path, 'userdict')
    # #
    # o.load_manualwords(o.logisticswords_path,'l')
    # o.load_manualwords(o.productwords_path, 'p')
    # o.load_manualwords(o.servicewords_path, 's')
    # # # 优先级最高的是删除清单，在此清单中的词都不作为特殊词
    # o.load_manualwords(o.deletewords_path, 'del')
    # # 放弃部分显示，以多显示为主
    # o.load_manualwords(o.degree_path, 'degree')
    # #
    # o.load_manualwords(o.adjectives_path, 'adjectives')

    o.close_db()
