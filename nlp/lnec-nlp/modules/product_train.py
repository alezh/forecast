import os,db,datetime,sys
import jieba
jieba.load_userdict(os.path.dirname(os.path.dirname(__file__))+"/dicts/userdict.txt")
import jieba.posseg as psg

class ProductProc():

    db = db.MyDb.initConn()
    cursor = db.cursor()

    def del_table(self,tablename):
        # 先清除临时表中的数据
        # tablename = 'special_wordlist_temp'
        self.cursor.execute('DELETE FROM %s' % (tablename))
        self.db.commit()  # 提交数据

    # 第1步，将产品全集数据分词，作为特殊清单的一部分
    def gen_productwords(self):

        # sql = "select * from product_ln where code = 'AYTF051-2S'"
        # 1) 获取来自产品表的分词数据，进入临时表special_wordlist_temp
        sql = "select * from product_ln"
        self.cursor.execute(sql)
        rslt = self.cursor.fetchall()
        for r in rslt:
            for i in range(2,len(r)):
                str1 = str(r[i])
                str1 = str1.replace("'","")
                str1 = str1.replace('"', '')
                str1 = str1.replace("\n", "")
                str1 = str1.replace("\r", "")
                if r[i] != '':
                    seg_gen1 = psg.cut(str1)
                    for word, flag in seg_gen1:
                        # print (word +' ' +flag+ '  ')
                        # 重复的则不插入
                        try:
                            self.cursor.execute('SELECT * FROM special_wordlist_temp where word = "%s"' % word)
                            if self.cursor.rowcount == 0:
                                logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                                value = [word,flag,"p","product_ln",logtime]
                                self.cursor.execute('insert into special_wordlist_temp(word,charactertype,wordtype,src,lstupdtime) VALUES (%s,%s,%s,%s,%s)',value)
                                self.db.commit()
                        except Exception:
                            self.db.rollback()

        # 2) 处理special_wordlist_temp 中的非正则数据，使用sql处理
        self.cursor.execute(
            "update special_wordlist_temp  set wordtype = 'other' where word   REGEXP '[A-Z0-9.]'")
        self.db.commit()
        self.cursor.execute(
            "update special_wordlist_temp set wordtype = 'other'  where wordtype <>'other'  and charactertype = 'x'")
        self.db.commit()

        # 3) 将临时表special_wordlist_temp中的数据转移进入正式表special_wordlist
        sql = "insert into special_wordlist(word,charactertype,wordtype,src,lstupdtime) " \
              "select word,charactertype,'p',src,lstupdtime from special_wordlist_temp where src = 'product_ln' and wordtype ='p' "
        self.cursor.execute(sql)
        self.db.commit()

    # 省略此步，将评论数据分词，处理到临时表special_wordlist_temp中 省略
    # def gen_commentwords(self):
    #
    #     # 获取来自评论表的分词数据
    #     # sql = "select content from comment_train where id = 20003"
    #     sql = "select content from comment_train"
    #     self.cursor.execute(sql)
    #     rslt = self.cursor.fetchall()
    #     for r in rslt:
    #         # print(r[0])
    #         if r[0] != '':
    #             seg_gen1 = psg.cut(r[0])
    #             for word, flag in seg_gen1:
    #                 # print (word +' ' +flag+ '  ')
    #                 # 重复的则不插入
    #                 try:
    #                     self.cursor.execute('SELECT * FROM special_wordlist_temp where word = "%s"' % word)
    #                     if self.cursor.rowcount == 0:
    #                         logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #                         value = [word,flag,"","comment_train",logtime]
    #                         self.cursor.execute('insert into special_wordlist_temp(word,charactertype,wordtype,src,lstupdtime) VALUES (%s,%s,%s,%s,%s)',value)
    #                         self.db.commit()
    #                 except Exception:
    #                     self.db.rollback()

    def close_db(self):
        self.cursor.close()
        self.db.close()


if __name__ == "__main__":  # 主程序
    o = ProductProc()
    # o.del_table("special_wordlist")
    # o.gen_productwords()

    o.close_db()




