import os,time,db,datetime
import jieba
jieba.load_userdict(os.path.dirname(os.path.dirname(__file__))+"/dicts/userdict.txt")
import jieba.posseg as psg
from modules import logs
import sys

class CommentPhraseProc():
    db = db.MyDb.initConn()
    log = logs.MyLogs()
    cursor = db.cursor()
    # 创建停用词列表
    # stopwords = [line.strip() for line in
    #              open(os.path.dirname(os.path.dirname(__file__)) + '/dicts/stopwords.txt', encoding='UTF-8').readlines()]
    negative = [line.strip() for line in
                 open(os.path.dirname(os.path.dirname(__file__)) + '/dicts/negative.txt', encoding='UTF-8').readlines()]


    # 清空comment_train_phrase
    def del_comment_train_phrase(self,id):
        # print(id)
        tablename = 'comment_train_phrase'
        if id == '':
            self.cursor.execute("DELETE FROM %s" % (tablename))
        else:
            self.cursor.execute(
                "delete from comment_train_phrase where comment_train_id in (%s) " % (id))
        self.db.commit()


    # 第1步 处理词性组合

    def psg_comment_phrase(self,id):
        if id == '':
            sql = "select * from comment_train where id in (select id from tmp_scf_phrase)"
        else:
            sql = "select * from comment_train where id in (" + str(id) + ")"


        self.cursor.execute(sql)
        rslt = self.cursor.fetchall()
        for r in rslt:
            commentid = r[0]
            self.cursor.execute("select word,charactertype,id,'' from comment_train_wordlist where comment_train_id = %s order by id" %(commentid))
            rslt2 = self.cursor.fetchall()
            seg_gen2 = []
            for r2 in rslt2:
                seg_gen2.append(list(r2))
            # print(seg_gen2)
            self.psg_comment_phrase_type(commentid, seg_gen2)

    def psg_comment_phrase_type(self,commentid,seg_gen):
        i = 0
        l = len(seg_gen)
        # 限定在前后找n个字符之内
        delta = 5
        # print(commentid)
        for word,flag,id,note in seg_gen:
            isfound = 0
            # 分出来的名词或形容词必须在特殊词清单中存在
            self.cursor.execute("select * from special_wordlist where word = '%s'" % (word))
            rows = self.cursor.rowcount
            if (flag == 'a' or flag == 'ad') and rows == 1:
                # print(word, flag)
                # 1、找到a之后，先往前找n
                j = i
                if i-delta < 0 :
                    endnum = 0
                else:
                    endnum = i-delta
                # for j in range(j,endnum,-1):
                while j>endnum:

                    # 判断往前是否是组合：程度词+否定词+形容词
                    if j==i and seg_gen[j-1][0] in self.negative and seg_gen[j - 2][1] in ['zg', 'vn', 'd']:
                        word = seg_gen[j-2][0] + seg_gen[j-1][0] + word
                        seg_gen[j - 1][3] = 'used'
                        seg_gen[j - 2][3] = 'used'
                        j = j -2
                    # 判断往前的是组合：程度词+形容词
                    if j==i and seg_gen[j-1][1] in ['zg','vn','d']:

                        word = seg_gen[j-1][0]+word
                        seg_gen[j - 1][3] = 'used'
                        # print(word)
                        j = j-1
                    # 先往前偏移量找一下是否存在否定词，若有则加上此否定词.已经使用过的否定词，不再加上。
                    if seg_gen[j-1][0] in self.negative and seg_gen[j-1][3] != 'used':
                        word = seg_gen[j-1][0]+word
                        # print(word)

                    # print(seg_gen[j-1][0])
                    # 名词必须在特殊词清单中
                    self.cursor.execute("select * from special_wordlist where charactertype = 'n' and word = '%s'" % (seg_gen[j-1][0]))
                    rows = self.cursor.rowcount
                    if seg_gen[j-1][1] == 'n' and rows == 1:
                        # print(seg_gen[j-1][0]+' '+seg_gen[i][0])
                        # value = [commentid, 'n'+flag,seg_gen[j-1][2],seg_gen[j-1][0],seg_gen[i][2],seg_gen[i][0], seg_gen[j-1][0]+seg_gen[i][0], 'p',time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())]
                        value = [commentid, 'n' + flag, seg_gen[j - 1][2], seg_gen[j - 1][0], seg_gen[i][2],
                                 word, seg_gen[j - 1][0] + word, 'p',
                                 time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())]
                        # 如果已存在a1n/ad1n组合，则拆分原词组，只显示a1/ad1；以na/nad组合优先
                        # self.cursor.execute('delete from comment_train_phrase where comment_train_id = %s and charactertype not in ("na","nad") and noun = "%s"' %(commentid,seg_gen[j-1][0]))
                        self.cursor.execute(
                            "update comment_train_phrase set nounid = 0,noun = '' ,phrase = adword where comment_train_id = %s and charactertype not in ('na','nad') and nounid = '%s'" % (
                            commentid, seg_gen[j - 1][2]))
                        self.db.commit()

                        self.cursor.execute("select * from comment_train_phrase  where comment_train_id = %s and charactertype in ('na','nad') and noun = '%s' and nounid = %s" %(commentid,seg_gen[j-1][0],seg_gen[j-1][2]))
                        rows = self.cursor.rowcount
                        # 如果不存在na或nad组合，就插入
                        if rows == 0:
                            try:
                                self.cursor.execute(
                                    "insert into comment_train_phrase(comment_train_id,charactertype,nounid,noun,adwordid,adword,phrase,phrasetype,lstupdtime) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",value)
                                self.db.commit()
                            except Exception:
                                self.log.save_log('comment_train_phrase', '插入异常，id=' + str(commentid) + ' ' + r[0])
                                self.db.rollback()
                            isfound = 1
                            break
                        # 如果存在na或nad组合，就记为未找到n，先不处理
                        else:
                            isfound = 0
                            # 此处退出循环的目的是为了避免在n1n2a1a2的组合下，形成n1a2组合
                            break
                    j = j-1
                # 2、如果前面没有找到n，再往后找一次
                if isfound == 0:
                    j = i
                    if i+delta>l:
                        endnum = l
                    else:
                        endnum = i+delta
                    # for j in range(j, endnum-1 , 1):
                    while j<endnum-1:
                        # 名词必须在特殊词清单中
                        self.cursor.execute("select * from special_wordlist where charactertype = 'n' and word = '%s'" % (seg_gen[j + 1][0]))
                        rows = self.cursor.rowcount
                        if seg_gen[j+1][1] == 'n' and rows == 1:
                            # print(seg_gen[j][0] + ' ' + seg_gen[i][0])
                            value = [commentid, flag + 'n' , seg_gen[j+1][2],seg_gen[j+1][0],seg_gen[i][2],word,word + seg_gen[j+1][0],'p',time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())]

                            # 如果已存在an/adn组合，则不显示n，只显示a/ad；用最新的a/ad去组成an/adn
                            self.cursor.execute(
                                "update comment_train_phrase set nounid = 0,noun = '' ,phrase = adword where comment_train_id = %s and charactertype not in ('na','nad') and nounid = '%s'" % (
                                    commentid, seg_gen[j + 1][2]))
                            self.db.commit()

                            try:
                                self.cursor.execute(
                                    "insert into comment_train_phrase(comment_train_id,charactertype,nounid,noun,adwordid,adword,phrase,phrasetype,lstupdtime) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",value)
                                self.db.commit()
                            except Exception:
                                self.log.save_log('comment_train_phrase', '插入异常，id=' + str(commentid) + ' ' + r[0])
                                self.db.rollback()
                            isfound = 1
                            break
                        j = j + 1
                # 3、如果后面也没有找到n，那么就判断:如果在形容词在特殊清单中，则取出。
                if isfound == 0:
                    # print(word)
                    self.cursor.execute("select * from special_wordlist where src <> 'negativeevaluation'"
                                        "and src <> 'positiveevaluation' and word = '%s'" % (seg_gen[i][0]))
                    rows = self.cursor.rowcount
                    if rows == 1:
                        value = [commentid, flag,  seg_gen[i][2],
                                 word, word, 'p',
                                 time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())]
                        try:
                            self.cursor.execute(
                                "insert into comment_train_phrase(comment_train_id,charactertype,adwordid,adword,phrase,phrasetype,lstupdtime) VALUES (%s,%s,%s,%s,%s,%s,%s)",
                                value)
                            self.db.commit()
                        except Exception:
                            self.log.save_log('comment_train_phrase', '插入异常，id=' + str(commentid) + ' ' + r[0])
                            self.db.rollback()
            i = i + 1

    # 第2步 处理特殊词：1） “速度”的特殊词，若前一个词是属于l还是s？
    def proc_specilaword_single(self,word):
        if word == '速度' or word == '回复' or word == '答复' or word == '回答' or word == '态度':
            print('开始处理:'+ word)
            # sql = "select * from comment_train_phrase where noun ='速度' and comment_train_id =28192"
            self.cursor.execute("select id,comment_train_id from comment_train_phrase where noun = '%s'  order by comment_train_id" %(word))
            rslt = self.cursor.fetchall()
            for r in rslt:
                # print(r[1])
                self.cursor.execute("select id,comment_train_id,word from comment_train_wordlist where comment_train_id = %s" %(r[1]))
                rslt2 = self.cursor.fetchall()
                i=0
                for r2 in rslt2:
                    if r2[2] == word and i>=1 :
                        # 必须在特殊词清单中
                        self.cursor.execute("select * from special_wordlist where charactertype = 'n' and  word = '%s'" % (rslt2[i-1][2]))
                        if self.cursor.rowcount == 1:
                            # print(r[0])
                            # print((rslt2[i-1][2]))
                            # 如果前一个名词被当做名词使用过了，则先删除
                            self.cursor.execute("delete from comment_train_phrase where  nounid = %s" % (rslt2[i - 1][0]))
                            self.db.commit()

                            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            self.cursor.execute("update comment_train_phrase  "
                                                "set noun = concat('%s','%s') , phrase = concat(noun,adword),lstupdtime = '%s'"
                                                "where  id = %s "%(rslt2[i-1][2],word,logtime,r[0]))
                            self.db.commit()
                    i=i+1
                # print(rslt2)
        if word == '值得':
            sql = "select id,comment_train_id,charactertype,nounid,noun,adwordid,adword from comment_train_phrase where adword ='值得' "
            self.cursor.execute(sql)
            rslt = self.cursor.fetchall()
            print('开始处理"值得"')
            for r in rslt:
                # print(r[1])
                # 如果值得的后面一个词是名词或动词，则组成  值得+n/v
                self.cursor.execute(
                    "select id,comment_train_id,word from comment_train_wordlist where comment_train_id =  %s and id = %s and charactertype in('v','n')" % (r[1],r[5]+1))
                rslt2 = self.cursor.fetchone()
                if self.cursor.rowcount == 1:

                    self.cursor.execute(
                        "update comment_train_phrase set nounid = '%s',noun = '%s' ,phrase = concat(adword, noun) where id =  %s" %(rslt2[0],rslt2[2],r[0]) )
                    self.db.commit()
        if word == '质量':
            sql = "select id,comment_train_id,charactertype,nounid,noun,adwordid,adword from comment_train_phrase " \
                  "where noun = '质量' and adword ='问题' "
            self.cursor.execute(sql)
            rslt = self.cursor.fetchall()
            print('开始处理 质量 ')
            for r in rslt:
                # print(r[1])
                endid = r[3]
                # 判断“质量问题”前面是否有否定词
                self.cursor.execute("select id,comment_train_id,word from comment_train_wordlist where comment_train_id = '%s'"  % (r[1]))
                rslt2 = self.cursor.fetchall()
                startid = rslt2[0][0]
                # 往前的偏移量3
                dlt = min(endid - startid,3)
                i = 0
                for r2 in rslt2:
                    if r2[0] == endid:
                        for j in range(i,i-dlt,-1):
                            if rslt2[j-1][2] in self.negative:
                                word = rslt2[j-1][2]+'质量'
                                self.cursor.execute(
                                    "update comment_train_phrase set noun = '%s' ,phrase = concat(noun,adword) where id =  %s" % (
                                    word,  r[0]))
                                self.db.commit()
                    i = i + 1


    def show_rslt(self,id):
        sql = "select content,content1 from comment_train where id in " + str(id)
        self.cursor.execute(sql)
        self.db.commit()
        rslt = self.cursor.fetchall()
        # for a,b in rslt:
        #     print(a)
        #     print(b)

        sql = "select phrase from comment_train_phrase where comment_train_id in (" + str(id) + ")"
        self.cursor.execute(sql)
        self.db.commit()
        rslt2 = self.cursor.fetchall()
        # for r in rslt2:
        #    print(r)

    def close_db(self):
        self.cursor.close()
        self.db.close()


if __name__ == "__main__":  # 主程序
    if len(sys.argv) == 1:
        v_id = ''
    else:
        v_id = "'" + str(sys.argv[1]).replace(',', '\',\'').replace('(', '').replace(')', '') + "'"

    o = CommentPhraseProc()

    #o.del_comment_train_phrase(v_id)
    o.psg_comment_phrase(v_id)

    if v_id == '':
        o.proc_specilaword_single('速度')
        o.proc_specilaword_single('回复')
        o.proc_specilaword_single('回答')
        o.proc_specilaword_single('答复')
        o.proc_specilaword_single('态度')
        o.proc_specilaword_single('值得')
        o.proc_specilaword_single('质量')


    # o.show_rslt('')
    o.close_db()