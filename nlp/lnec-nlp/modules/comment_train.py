import os,time,db,datetime
import jieba
jieba.load_userdict(os.path.dirname(os.path.dirname(__file__))+"/dicts/userdict.txt")
import jieba.posseg as psg
from modules import logs
import sys

class CommentProc():

    db = db.MyDb.initConn()
    log = logs.MyLogs()
    cursor = db.cursor()
    # 创建停用词列表
    stopwords = [line.strip() for line in open(os.path.dirname(os.path.dirname(__file__))+'/dicts/stopwords.txt', encoding='UTF-8').readlines()]
    # 处理一些异常字符
    def proc_comment_train(self):
        self.cursor.execute("update comment_train set content = REPLACE(content,E'\\'',' ')")
        self.cursor.execute("update comment_train set content = REPLACE(content,'\"',' ')")
        self.cursor.execute("update comment_train set content = REPLACE(content,'	',' ')")
        self.cursor.execute("UPDATE comment_train SET  content = REPLACE(content, CHR(10), '')")
        self.cursor.execute("UPDATE comment_train SET  content = REPLACE(content, CHR(13), '')")
        self.db.commit()
    # 对句子进行分词,返回字符串
    def seg_sentence(self,sentence):
        sentence_seged = psg.cut(sentence.strip())
        outstr = ''
        for word,flag in sentence_seged:
            if word not in self.stopwords:
                if word != '\t':
                    outstr += word+"/"+flag+" "
        return outstr

    # 对句子进行分词,返回list类型
    def seg_sentence_list(self,sentence):
        seg_gen1 = psg.cut(sentence)
        wordlist = []
        for word, flag in seg_gen1:
            if word not in self.stopwords and word !='\\':
                wordlist.append([word, flag])
        return wordlist

    # 将分词的文本结果，存储在comment_train中
    def psg_comment_train(self,id):
        if id == '':
            sql = "select id,content from comment_train order by id"
        else:
            sql = "select id,content from comment_train where id in (" + str(id) + ") order by id"
        self.cursor.execute(sql)
        rslt = self.cursor.fetchall()
        for r in rslt:
            commentid = r[0]
            # print(commentid)
            outstring = self.seg_sentence(r[1])
            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.cursor.execute("update comment_train set content1 = '%s' ,lstupdtime = '%s' where id = %s  " %(outstring,logtime ,commentid))
            self.db.commit()  # 提交数据

    # 将分词的清单结果，存储在comment_train_wordlist中
    def psg_comment_train_wordlist(self,id):
        if id == '':
            sql = "select id,content from comment_train where id in (select id from tmp_scf_comment_train) order by id"
        else:
            sql = "select id,content from comment_train where id in (" + str(id) + ")"
        self.cursor.execute(sql)
        rslt = self.cursor.fetchall()
        for commentid,content in rslt:
            # print(commentid)
            rslt = self.seg_sentence_list(content)
            for r in rslt:
                logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                value = [commentid, r[0], r[1],logtime]
                # print(value)
                try:
                    self.cursor.execute(
                        "insert into comment_train_wordlist(comment_train_id,word,charactertype,lstupdtime) VALUES (%s,%s,%s,%s)",value)
                    self.db.commit()
                except Exception:
                    self.log.save_log('comment_train','插入异常，id='+str(commentid)+' '+r[0])
                    self.db.rollback()

        # 清空comment_train_phrase

    def del_comment_train_wordlist(self,id):
        tablename = 'comment_train_wordlist'
        if id == '':
            self.cursor.execute("DELETE FROM %s" % (tablename))
        else:
            self.cursor.execute("DELETE FROM %s where comment_train_id in (%s)" % (tablename,id))
        self.db.commit()

    def del_comment_train_wordlist_2(self):
        self.cursor.execute("delete from comment_train_wordlist  where word !='EMS' AND word ~ '[A-Z0-9.]' ")
        self.db.commit()

    def close_db(self):
        self.cursor.close()
        self.db.close()

if __name__ == "__main__":  # 主程序

    if len(sys.argv) == 1:
        v_id = ''
    else:
        v_id = "'" + str(sys.argv[1]).replace(',', '\',\'').replace('(', '').replace(')', '') + "'"

    o = CommentProc()
    '''
    if v_id == '':
        o.proc_comment_train()

    o.psg_comment_train(v_id)
    if v_id != '':
        o.del_comment_train_wordlist(v_id)
    '''
    o.psg_comment_train_wordlist(v_id)
    o.del_comment_train_wordlist_2()

    o.close_db()

