import os,time,db,datetime
from modules import logs
import comment_train as ct
import sys

class SentimentProc():
    db = db.MyDb.initConn()
    oct = ct.CommentProc()
    log = logs.MyLogs()
    cursor = db.cursor()
    negative = [line.strip() for line in
                open(os.path.dirname(os.path.dirname(__file__)) + '/dicts/negative.txt', encoding='UTF-8').readlines()]

    def del_comment_train_sentiment(self,id):
        tablename = 'comment_train_sentiment'
        if id == '':
            self.cursor.execute("DELETE FROM %s" % (tablename))
        else:
            self.cursor.execute("DELETE FROM %s where comment_train_id in (%s)" % (tablename,id))
        self.db.commit()

    def sentiment_phrase(self, id):
        if id == '':
            sql = "select a.id, a.comment_train_id, a.adword, a.noun, a.adwordid, a.nounid, b.charactertype from comment_train_phrase a join comment_train_wordlist b on a.comment_train_id = b.comment_train_id and a.adwordid = b.id and a.comment_train_id in (select id from tmp_scf_comment_train)"
        else:
            sql = "select a.id, a.comment_train_id, a.adword, a.noun, a.adwordid, a.nounid, b.charactertype from comment_train_phrase a join comment_train_wordlist b on a.comment_train_id = b.comment_train_id and a.adwordid = b.id where a.comment_train_id in (" + str(id) + ")"
        self.cursor.execute(sql)
        rslt = self.cursor.fetchall()
        for r in rslt:
            phraseid = r[0]
            commentid = r[1]
            adword = r[2]
            wordtype = r[6]
            noun = r[3]
            value = [commentid,phraseid,"",1,"",1,"",1,0]
            # print(adword, wordtype)
            self.cursor.execute(
                "insert into comment_train_sentiment(comment_train_id,comment_train_phrase_id,negative,negative_score,degree,degree_score,adjective,adjective_score,phrase_score) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",value)
            self.db.commit()
            # adword_list = self.oct.seg_sentence_list(adword)

            # print(adword_list)
            # for word,wordtype in adword_list:
            if adword in self.negative:
                sql = "update comment_train_sentiment set negative = concat(negative, %s),negative_score=negative_score*(-1) where comment_train_phrase_id = %s"
                self.cursor.execute(sql,(adword,phraseid))
                self.db.commit()
                continue
            if wordtype in ['zg','vn','d']:
                sql = "update comment_train_sentiment set degree = concat(degree, %s) where comment_train_phrase_id = %s"
                self.cursor.execute(sql, (adword, phraseid))
                self.db.commit()
                continue
            if wordtype in ['a','ad']:

                sql = "update comment_train_sentiment set adjective = concat(adjective, %s) where comment_train_phrase_id = %s"

                self.cursor.execute(sql, (adword, phraseid))

                self.db.commit()
                continue

            if  adword =='问题':
                # 判断“质量问题”前面是否有否定词
                endid = r[4]
                self.cursor.execute(
                    "select id,comment_train_id,word from comment_train_wordlist where comment_train_id = '%s' order by id" % (
                        commentid))
                rslt2 = self.cursor.fetchall()
                startid = rslt2[0][0]
                # 往前的偏移量5
                dlt = min(endid - startid, 5)
                i = 0
                for r2 in rslt2:
                    if r2[0] == endid:
                        for j in range(i, i - dlt, -1):
                            if rslt2[j - 1][2] in self.negative:
                                sql = "update comment_train_sentiment set negative = concat(negative, %s),negative_score=negative_score*(-1) where comment_train_phrase_id = %s"
                                self.cursor.execute(sql, (rslt2[j - 1][2], phraseid))
                                self.db.commit()
                    i = i + 1



    def sentiment_phrase_score(self,id):

        print('开始程度词评分  '+ datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        if id == '':
            # 程度词评分
            sql = "update comment_train_sentiment a set degree_score = b.score from degree_wordlist b where a.degree =b.word "
            self.cursor.execute(sql)
            self.db.commit()
            # 形容词的褒义贬义分分值及混杂程度词的调整权重
            print('开始形容词评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            sql = "update comment_train_sentiment a set adjective_score = b.wordscore*b.wordscoreadjust from special_wordlist b where a.adjective =b.word "
            self.cursor.execute(sql)
            self.db.commit()
            # 词组评分
            print('开始词组评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            # logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            sql = "update comment_train_sentiment set phrase_score = negative_score * degree_score * adjective_score ,lstupdtime = now()"
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_sentiment set phrase_score = 2 where phrase_score > 2"
            self.cursor.execute(sql)
            self.db.commit()
            sql = "update comment_train_sentiment set phrase_score = -2 where phrase_score < -2"
            self.cursor.execute(sql)
            self.db.commit()


            # 评论评分
            print('开始评论评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            logtime1 = datetime.datetime.now()
            sql = "update comment_train a set  score = b.score, lstupdtime = now() " \
                  " from (select  comment_train_id, round(avg(phrase_score), 2) score from comment_train_sentiment GROUP BY comment_train_id) b" \
                  "  where  a.id = b.comment_train_id"
            self.cursor.execute(sql)
        else:
            # 程度词评分
            sql = "update comment_train_sentiment a set degree_score = b.score from degree_wordlist b where a.degree =b.word and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()
            # 形容词的褒义贬义分分值及混杂程度词的调整权重
            print('开始形容词评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            sql = "update comment_train_sentiment a set adjective_score = b.wordscore*b.wordscoreadjust from special_wordlist b where a.adjective =b.word and a.comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()
            # 词组评分
            print('开始词组评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            # logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            sql = "update comment_train_sentiment set phrase_score = negative_score * degree_score * adjective_score ,lstupdtime = now() where comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()

            sql = "update comment_train_sentiment set phrase_score = 2 where phrase_score > 2 and comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()
            sql = "update comment_train_sentiment set phrase_score = -2 where phrase_score < -2 and comment_train_id in (%s)" %(id)
            self.cursor.execute(sql)
            self.db.commit()

            # 评论评分
            print('开始评论评分  ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            logtime1 = datetime.datetime.now()
            sql = "update comment_train a set  score = b.score, lstupdtime = now() " \
                  " from (select  comment_train_id, round(avg(phrase_score), 2) score from comment_train_sentiment GROUP BY comment_train_id) b" \
                  "  where  a.id = b.comment_train_id and a.id in (%s)" %(id)
            self.cursor.execute(sql)
        self.db.commit()


    def close_db(self):
        self.cursor.close()
        self.db.close()

if __name__ == "__main__":  # 主程序
    if len(sys.argv) == 1:
        v_id = ''
    else:
        v_id = "'" + str(sys.argv[1]).replace(',', '\',\'').replace('(', '').replace(')', '') + "'"

    o = SentimentProc()
    #o.del_comment_train_sentiment(v_id)
    o.sentiment_phrase(v_id)
    o.sentiment_phrase_score(v_id)
    o.close_db()

