import os,time,db,datetime


class MyLogs():
    db = db.MyDb.initConn()
    cursor = db.cursor()
    def save_log(self,logmodule,content):
        try:
            logtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            value = [logmodule,content,logtime]
            self.cursor.execute('insert into logs(logmodule,content,lstupdtime) VALUES (%s,%s,%s)',value)
            self.db.commit()
        except Exception:
            self.db.rollback()


if __name__ == "__main__":  # 主程序
    o = MyLogs()
    o.save_log('comment_train','插入异常，id='+str(31462))