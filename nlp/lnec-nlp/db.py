# import os,pymysql
import os,psycopg2


class  MyDb():

    # 使用mysql
    # def initConn():
    #     config = {
    #         "host": "127.0.0.1",
    #         "user": "root",
    #         "password": "123456",
    #         "db": "nlp",
    #         'charset': 'utf8'
    #     }
    #     return pymysql.connect(**config)

    # 使用postgresql
    def initConn():
        config = {
            "host": "10.4.37.103",
            "user": "nlp",
            "password": "123456",
            "database": "nlp",
            "port":5432
        }
        return psycopg2.connect(**config)

        # return psycopg2.connect(host="127.0.0.1",port=5432,user="root",password="root",database="nlp")



if __name__ == "__main__":  # 主程序
    # o = CommentProc()
    db = MyDb.initConn()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM comment_train LIMIT 10")
    rows = cursor.fetchall()
    print(rows)
    db.commit()

    db.close()
