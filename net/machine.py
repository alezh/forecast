import xgboost
import lightgbm as lgb
from sklearn import metrics


# xgboost
class XgbLearning(object):
    param = {'max_depth': 5, 'gamma': 0.1, 'eta': 0.1, 'nthread': 4, 'seed': 710}
    params = {'booster': 'gbtree',
              'objective': 'binary:logistic',
              'eval_metric': 'auc',
              'max_depth': 4,
              'lambda': 10,
              'subsample': 0.75,
              'colsample_bytree': 0.75,
              'min_child_weight': 2,
              'eta': 0.025,
              'seed': 0,
              'nthread': 8,
              'silent': 1}
    model = None
    preds = None

    def __init__(self, param={}):
        if len(param) > 0:
            self.param = param
        pass

    def prototype_model(self, train, label_x, test, n_round=10):
        train_x = xgboost.DMatrix(train, label=label_x)
        test_x = xgboost.DMatrix(test)
        self.model = xgboost.train(self.param, train_x, num_boost_round=n_round)

    def predict(self, test_x):
        self.preds = self.model.predict(test_x)
        return self.preds

    def regressor(self, md=5, lr=0.01, n_est=100, nt=4, silent=True, objective='reg:linear'):
        self.model = xgboost.XGBRegressor(max_depth=md, learning_rate=lr, n_estimators=n_est, nthread=nt, silent=silent,
                                          objective=objective)

    def classifier(self, md=5, lr=0.1, n_est=100, nt=4, silent=True, objective='reg:gamma'):
        self.model = xgboost.XGBRegressor(max_depth=md, learning_rate=lr, n_estimators=n_est, nthread=nt, silent=silent,
                                          objective=objective)

    def fit(self, x, y):
        self.model.fit(x, y)

    def accuracy(self, test_y):
        return 'ACC: %.4f' % metrics.accuracy_score(test_y, self.preds)


# lightgbm
class LgbmLearning(object):
    def __init__(self):
        pass
