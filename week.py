import torch
from torch import nn
import pandas as pd
import lightgbm as lgb
import xgboost as xgb
import matplotlib.pyplot as plt


from utils.Read import PgSqlToCsv, train_test_split

cols = ['week', 'year', 'month', 'total', 'discount', 'dis', 'price', 'spu_tm', 'hao',
        'p', 'c', 'h', 'pv', 'uv', 'sc', 'jg', 'WeeksAgoNum', 'twoWeeksAgoNum', 'threeWeeksAgoNum']
sales = pd.read_csv('cache/train/AA035409.csv')
train_x, train_y, test_x, test_y = train_test_split(sales, 0.8, 'total')
