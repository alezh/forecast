from utils.pgsql import CONN
import unicodecsv as csv
import pandas as pd


class Evaluate:

    def __init__(self):
        pass

    # 按周获取评价
    def pgInit(self):
        data = []
        sql1 = "SELECT pj.week,pj.year,sum(pj.hao),sum(pj.ping),sum(pj.cha),AVG(hf),count(*),AVG(hao),AVG(ping),avg(cha)"
        sql = "SELECT EXTRACT(week FROM 评论时间 ):: INTEGER AS week, EXTRACT(YEAR FROM 评论时间 ):: INTEGER AS YEAR "
        sql += ",CASE WHEN 评论得分>0 THEN 1 ELSE 0 END AS hao,CASE WHEN 评论得分=0 THEN 1 ELSE 0 END AS ping,"
        sql += "CASE WHEN 评论得分<0 THEN 1 ELSE 0 END AS cha,CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END AS hf "
        sql += "FROM 销售预测_评论数据 WHERE "
        sql += "spu_tm = 'AA000006' ORDER BY year"
        sql1 += " FROM (" + sql + ")as pj GROUP BY pj.week,pj.year ORDER BY pj.year,pj.week"
        cursor = CONN.cursor()
        cursor.execute(sql1)
        rows = cursor.fetchall()
        for row in rows:
            data.append(EvaluateList(row))
        cursor.close()
        return data

    def evaCsvInsert(self):
        eva = self.pgInit()
        evaHead = ('week', 'year', 'hao', 'zhong', 'cha', 'hf', 'avgH', 'avgZ', 'avgC', 'rateH', 'rateZ', 'rateC')
        writer = csv.DictWriter(open('cache/eva.csv', 'wb'), evaHead)
        writer.writeheader()
        for e in eva:
            writer.writerow({
                'week': e.week,
                'year': e.year,
                'hao': e.hao,
                'zhong': e.zhong,
                'cha': e.cha,
                'hf': e.hf,
                'avgH': e.avgH,
                'avgZ': e.avgZ,
                'avgC': e.avgC,
                'rateH': e.rateH,
                'rateZ': e.rateZ,
                'rateC': e.rateC
            })

    def train_pd(self):
        sql = "SELECT week,year,sum(hao)as hao,sum(ping) as p,sum(cha) as c,sum(hf) as h,spu_tm, "
        sql += "AVG(hao)::NUMERIC(19,2) as ahao,AVG(ping)::NUMERIC(19,2) as ap,avg(cha)::NUMERIC(19,2) as ac FROM"
        sql += " (SELECT EXTRACT(week FROM 评论时间)::INTEGER AS week,EXTRACT(YEAR FROM 评论时间)::INTEGER AS year,"
        sql += "CASE WHEN 评论得分>0 THEN 1 ELSE 0 END AS hao,CASE WHEN 评论得分=0 THEN 1 ELSE 0 END AS ping,"
        sql += "CASE WHEN 评论得分<0 THEN 1 ELSE 0 END AS cha,CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END AS hf, "
        sql += "spu_tm "
        sql += "FROM 销售预测_评论数据_20200420 ORDER BY year) as pj GROUP BY week,year,spu_tm ORDER BY week,year"
        return pd.read_sql(sql, CONN)

    def daily_pd(self, code="AA000006"):
        sql = "SELECT to_char(评论时间,'YYYYMMDD')::INTEGER AS days,to_char(评论时间,'MM-DD')AS day,"
        sql += "EXTRACT(week FROM 评论时间 ) :: INTEGER AS week,EXTRACT (YEAR FROM 评论时间 ) :: INTEGER AS year,"
        sql += "sum(CASE WHEN 评论得分>0 THEN 1 ELSE 0 END) AS hao,	sum(CASE WHEN 评论得分=0 THEN 1 ELSE 0 END) AS ping,"
        sql += "sum(CASE WHEN 评论得分<0 THEN 1 ELSE 0 END) AS cha,	sum(CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END) AS hf,"
        sql += "avg(CASE WHEN 评论得分>0 THEN 1 ELSE 0 END)::NUMERIC(19,4) AS avghao,"
        sql += "avg(CASE WHEN 评论得分=0 THEN 1 ELSE 0 END)::NUMERIC(19,4) AS avgping,"
        sql += "avg(CASE WHEN 评论得分<0 THEN 1 ELSE 0 END)::NUMERIC(19,4) AS avgcha,"
        sql += "avg(CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END)::NUMERIC(19,4) AS avghf"
        sql += " FROM 销售预测_评论数据 WHERE spu_tm = '" + code + "' GROUP BY days,week,year,day ORDER BY days"
        return pd.read_sql(sql, CONN)


class EvaluateList:
    week = 0  # 周
    year = 0  # 年
    hao = 0  # 好评
    zhong = 0  # 中评
    cha = 0  # 差评
    hf = 0  # 回复
    avgH = 0  # 平均值
    avgZ = 0
    avgC = 0
    rateH = 0  # 累计好评率
    rateZ = 0  # 占有率
    rateC = 0  # 累计差评率

    def __init__(self, data):
        self.week = data[0]
        self.year = data[1]
        self.hao = data[2]
        self.zhong = data[3]
        self.cha = data[4]
        self.hf = data[5]
        self.rateH = self.hao / data[6] * 100
        self.rateZ = self.zhong / data[6] * 100
        self.rateC = self.cha / data[6] * 100
        self.avgH = data[7]
        self.avgZ = data[8]
        self.avgC = data[9]
