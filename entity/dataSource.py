import psycopg2
import pandas as pd
import numpy as np

from learning.processing import Processing


class DataSource(object):
    conn = None
    salesSource = None
    pageViewSource = None
    evaluateSource = None
    result = None

    def __init__(self, sql=True):
        if sql:
            self.conn = psycopg2.connect(database='ning', user='nanli', password='123456', host='172.16.10.229',
                                         port=5432)

    def spu_class(self, csv=False):
        sql = "SELECT spu_tm as spu,ec中类t FROM 销售预测_产品属性数据_20200420 GROUP BY spu_tm,ec中类t"
        data = pd.read_sql(sql, self.conn)
        if csv:
            data.to_csv('cache/source/SpuClass.csv', index=False)
        return data

    # 销售数据 人员合并
    # buy_order 用户订单数量
    def GetSalesSource(self, csv=False):
        sql = "SELECT to_char(日期, 'YYYY-MM-DD' ) AS days,to_char( 日期, 'MM-DD' ) AS DAY,"
        sql += "EXTRACT( week FROM 日期 ) :: INTEGER AS week,EXTRACT ( MONTH FROM 日期 ) :: INTEGER AS MONTH,"
        sql += "EXTRACT( YEAR FROM 日期 ) :: INTEGER AS YEAR,日期,订单号_tm,会员号_tm,sku_tm,省,"
        sql += "substr(sku_tm, 0, 9) as spu,ck.对应仓,sum(付款件数) as num,"
        sql += "CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN " \
               "avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS 优惠金额,"
        sql += "COUNT (*) AS buy_order FROM 销售预测_交易支付数据_20200420 as xl "
        sql += "LEFT JOIN 销售预测_地区仓库对应表 as ck ON ck.收货地= xl.省 "
        sql += "WHERE 支付时间 IS NOT NULL GROUP BY "
        sql += "日期,订单号_tm,会员号_tm,sku_tm,省,ck.对应仓"
        data = pd.read_sql(sql, self.conn)
        if csv:
            data.to_csv('cache/source/SalesSource.csv', index=False)
        return data

    def GetPageViewSource(self, csv=False):
        sql = "SELECT to_char( 日期, 'YYYY-MM-DD' ) AS days,to_char( 日期, 'MM-DD' ) AS DAY,spu_tm as spu,"
        sql += "EXTRACT ( week FROM 日期 ) :: INTEGER AS week,EXTRACT ( MONTH FROM 日期 ) :: INTEGER AS MONTH,"
        sql += "EXTRACT ( YEAR FROM 日期 ) :: INTEGER AS YEAR,pv,uv,收藏数,加购数,平均停留时长,页面跳出率,搜索引导访客数"
        sql += " FROM 销售预测_流量数据_20200420 ORDER BY 日期"
        data = pd.read_sql(sql, self.conn)
        if csv:
            data.to_csv('cache/source/PageViewSource.csv', index=False)
        return data

    def GetEvaluateSource(self, csv=False):
        sql = "SELECT to_char( 评论时间, 'YYYY-MM-DD' ) AS days,to_char( 评论时间, 'MM-DD' ) AS DAY,spu_tm as spu,"
        sql += "EXTRACT ( week FROM 评论时间 ) :: INTEGER AS week,EXTRACT ( MONTH FROM 评论时间 ) :: INTEGER AS MONTH,"
        sql += "EXTRACT ( YEAR FROM 评论时间 ) :: INTEGER AS YEAR,CASE WHEN 评论得分>0 THEN 1 ELSE 0 END AS hao,"
        sql += "CASE WHEN 评论得分=0 THEN 1 ELSE 0 END AS ping,CASE WHEN 评论得分<0 THEN 1 ELSE 0 END AS cha,"
        sql += "CASE WHEN 答复内容 IS NULL THEN 0 ELSE 1 END AS hf FROM 销售预测_评论数据_20200420  ORDER BY 评论时间"
        data = pd.read_sql(sql, self.conn)
        if csv:
            data.to_csv('cache/source/EvaluateSource.csv', index=False)
        return data

    def SourceToCsv(self):
        self.spu_class(True)
        self.GetSalesSource(True)
        self.GetPageViewSource(True)
        self.GetEvaluateSource(True)
        self.conn.close()
        return self

    def SalesSplit(self, key='spu'):
        sales = pd.read_csv('cache/source/SalesSource.csv').groupby(key)
        for i, s in sales:
            name = 'cache/sales/' + i + '.csv'
            s.to_csv(name, index=False)
        return self

    def GetMonthPageView(self, name=None, group=None):
        cols = ['days', 'day', 'spu', 'week', 'month', 'year', 'pv', 'uv', '收藏数', '加购数', '平均停留时长', '页面跳出率', '搜索引导访客数']
        self.pageViewSource = pd.read_csv('cache/source/PageViewSource.csv')[cols]
        agg = {'year': 'first', 'month': 'first', 'week': 'first', 'pv': 'sum', 'uv': 'sum', '收藏数': 'sum',
               '加购数': 'sum', '平均停留时长': 'mean', '页面跳出率': 'mean', '搜索引导访客数': 'mean'}
        if name is not None:
            self.pageViewSource = self.pageViewSource.query("spu == '" + name + "'").fillna(0)
        else:
            agg.update({'spu': 'first'})
        page_views = self.pageViewSource.groupby(group).agg(agg)
        page_views.index = list(range(len(page_views.index)))
        return page_views

    def GetMonthEvaluate(self, name=None, group=None):
        cols = ['days', 'day', 'week', 'month', 'year', 'hao', 'ping', 'cha', 'hf', 'spu']
        self.evaluateSource = pd.read_csv('cache/source/EvaluateSource.csv')[cols]
        agg = {'year': 'first', 'month': 'first', 'week': 'first', 'hao': 'sum', 'ping': 'sum', 'cha': 'sum'}
        if name is not None:
            self.evaluateSource = self.evaluateSource.query("spu == '" + name + "'")
        else:
            agg.update({'spu': 'first'})
        evaluate = self.evaluateSource.groupby(group).agg(agg)
        evaluate.index = list(range(len(evaluate.index)))
        return evaluate

    # 获取每月订单数量与销售数量
    def GetMonthSales(self, name, group):
        cols = ['days', 'day', 'week', 'month', 'year', 'spu', 'num', '优惠金额', 'buy_order']
        # cols = ['days', 'day', 'week', 'month', 'year', '日期', '订单号_tm', '会员号_tm', 'sku_tm', '省', 'spu', '对应仓',
        #         'num', '优惠金额', 'buy_order']
        self.salesSource = pd.read_csv('cache/sales/' + name + '.csv')[cols]
        agg = {'year': 'first', 'month': 'first', 'week': 'first', 'num': 'sum', '优惠金额': 'mean', 'buy_order': 'sum'}
        sales = self.salesSource.groupby(group).agg(agg)
        sales.index = list(range(len(sales.index)))
        return sales

    # 分类数据保存CSV
    def GetMonthMerge(self):
        group_by = ['year', 'month', 'week', 'spu']
        cols = ['days', 'day', 'week', 'month', 'year', 'spu', 'num', '优惠金额', 'buy_order']
        self.salesSource = pd.read_csv('cache/source/SalesSource.csv', low_memory=False)[cols]
        agg = {'year': 'first', 'month': 'first', 'week': 'first', 'spu': 'first', 'num': 'sum', '优惠金额': 'mean',
               'buy_order': 'sum'}
        self.salesSource = self.salesSource.groupby(group_by).agg(agg)
        self.salesSource.index = list(range(len(self.salesSource.index)))
        spu = pd.read_csv('cache/source/SpuClass.csv')
        page_views = self.GetMonthPageView(None, group_by)
        evaluate = self.GetMonthEvaluate(None, group_by)
        self.salesSource = pd.merge(self.salesSource, spu, on='spu')
        self.salesSource = pd.merge(self.salesSource, page_views, how='outer', on=group_by)
        self.result = pd.merge(self.salesSource, evaluate, how='outer', on=group_by).fillna(0)
        self.result['pay_rate'] = self.result.apply(lambda x: x['buy_order'] / x['pv'] * 100 if x['pv'] > 0 else 0.001,
                                                    axis=1)
        self.result.to_csv('cache/source/result_class_week.csv', index=False)
        for i, s in self.result.groupby('ec中类t'):
            name = 'cache/class/' + str(i) + '_week.csv'
            s.to_csv(name, index=False)

    # 数据合并
    def SourceMerge(self, name, csv=False, group=['month']):
        group_by = ['year']
        group_by.extend(group)
        page_views = self.GetMonthPageView(name, group_by)
        evaluate = self.GetMonthEvaluate(name, group_by)
        sales = self.GetMonthSales(name, group_by)
        frames = pd.merge(sales, page_views, how='outer', on=group_by)
        self.result = pd.merge(frames, evaluate, how='outer', on=group_by).fillna(0)
        # .replace(np.inf, 0)
        # self.result['WeeksAgoNum'] = self.result['num'].shift(1)
        # self.result['twoWeeksAgoNum'] = self.result['num'].shift(2)
        self.result['pay_rate'] = self.result.apply(lambda x: x['buy_order'] / x['pv'] * 100 if x['pv'] > 0 else 0.001,
                                                    axis=1)
        if csv:
            self.result.to_csv('cache/source/' + name + 'result.csv', index=False)
        return self.result

    # 数据移位
    def displacement(self, result, number=1):
        data = pd.DataFrame({})
        # 'year', 'month', 'spu', 'num', '优惠金额',
        cols = ['pv', 'uv', '收藏数', '加购数', '平均停留时长',
                '页面跳出率', '搜索引导访客数', 'hao', 'ping', 'cha', 'pay_rate']
        for s, v in result.groupby(['spu']):
            v['WeeksAgoNum'] = v['num'].shift(1).copy()
            v[cols] = v[cols].shift(number).reset_index(drop=True).copy().dropna(how='any', axis=0)
            data = data.append(v)
        # result[cols] = self.result[cols].shift(number).reset_index(drop=True)
        return data

    def GetTrainData(self, name, result=None):
        col = ['buy_order', 'pv', 'uv', 'hao', 'ping', 'cha', 'pay_rate']
        if result is None:
            result = self.SourceMerge(name, group=['month', 'week']).query("year != 2016")
        result = result.mask(result.sub(result.mean()).div(result.std()).abs().gt(3))
        result = result.fillna(method='ffill')
        train_y = result.query("year != 2020")['num'].reset_index(drop=True)
        train = result.query("year != 2020").drop(columns=['num', 'year', 'month', 'week']).reset_index(drop=True)
        test = result.query("year == 2020").drop(columns=['num', 'year', 'month', 'week']).reset_index(drop=True)
        test_y = result.query("year == 2020")['num'].reset_index(drop=True)
        features, features_test = Processing().features(train[col], test[col], col)
        t = [train, features.drop(columns=col)]
        t1 = [test, features_test.drop(columns=col)]
        train_x = pd.concat(t, axis=1, sort=False)
        test_x = pd.concat(t1, axis=1, sort=False)
        # ohe
        f = pd.get_dummies(train_x)
        test_f = pd.get_dummies(test_x)
        f, test_f = f.align(test_f, join='inner', axis=1)
        return f, train_y, test_f, test_y

    def save(self, train_sub, submission, name, k='xgb', s=False):
        result = self.SourceMerge(name, group=['month', 'week']).query("year != 2016")
        train = result.query("year != 2020").reset_index(drop=True)
        train[k] = train_sub
        test = result.query("year == 2020").reset_index(drop=True)
        test[k] = submission
        if s:
            train.to_csv('cache/source/xgbAA060243_train.csv', index=False)
            test.to_csv('cache/source/xgbAA060243_test.csv', index=False)
        return train, test

    def GetTrainClassDataWeek(self, name, result=None):
        col = ['buy_order', 'pv', 'uv', 'hao', 'ping', 'cha', 'pay_rate']
        if result is None:
            result = pd.read_csv('cache/class/' + name + '_day.csv', low_memory=False).drop(
                columns=['ec中类t'])
        # 排序
        # result = self.displacement(result)
        result = result.sort_values(['year', 'month']).query("year != 2016").reset_index(drop=True)
        train_ch = result.query("year != 2020")['spu'].copy().reset_index(drop=True)
        test_ch = result.query("year == 2020")['spu'].copy().reset_index(drop=True)
        result = result.drop(columns=['spu'])

        result = result.mask(result.sub(result.mean()).div(result.std()).abs().gt(3))
        result = result.fillna(method='ffill')
        # 获取训练数据
        train_y = result.query("year != 2020")['num'].reset_index(drop=True)
        train = result.query("year != 2020").drop(columns=['num', 'year']).reset_index(drop=True)
        test = result.query("year == 2020").drop(columns=['num', 'year']).reset_index(drop=True)
        test_y = result.query("year == 2020")['num'].reset_index(drop=True)
        train['spu'] = train_ch
        test['spu'] = test_ch
        features, features_test = Processing().features(train[col], test[col], col)

        t = [train, features.drop(columns=col)]
        t1 = [test, features_test.drop(columns=col)]
        train_x = pd.concat(t, axis=1, sort=False)
        test_x = pd.concat(t1, axis=1, sort=False)
        # ohe
        f = pd.get_dummies(train_x)
        test_f = pd.get_dummies(test_x)
        f, test_f = f.align(test_f, join='inner', axis=1)
        return f, train_y, test_f, test_y

    def GetTrainClassDataDay(self, name, result=None):
        col = ['buy_order', 'pv', 'uv', 'hao', 'ping', 'cha', 'pay_rate']
        if result is None:
            result = pd.read_csv('cache/class/' + name + '_day.csv', low_memory=False).drop(
                columns=['ec中类t', 'week', 'week_x', 'week_y'])
        # 排序
        # result = self.displacement(result)
        result = result.sort_values(['year', 'month', 'day']).query("year != 2016").reset_index(drop=True)
        train_ch = result.query("year != 2020")['spu'].copy().reset_index(drop=True)
        test_ch = result.query("year == 2020")['spu'].copy().reset_index(drop=True)
        result = result.drop(columns=['spu', 'day'])

        result = result.mask(result.sub(result.mean()).div(result.std()).abs().gt(3))
        result = result.fillna(method='ffill')
        # 获取训练数据
        train_y = result.query("year != 2020")['num'].reset_index(drop=True)
        train = result.query("year != 2020").drop(columns=['num', 'year']).reset_index(drop=True)
        test = result.query("year == 2020").drop(columns=['num', 'year']).reset_index(drop=True)
        test_y = result.query("year == 2020")['num'].reset_index(drop=True)
        train['spu'] = train_ch
        test['spu'] = test_ch
        features, features_test = Processing().features(train[col], test[col], col)

        t = [train, features.drop(columns=col)]
        t1 = [test, features_test.drop(columns=col)]
        train_x = pd.concat(t, axis=1, sort=False)
        test_x = pd.concat(t1, axis=1, sort=False)
        # ohe
        f = pd.get_dummies(train_x)
        test_f = pd.get_dummies(test_x)
        f, test_f = f.align(test_f, join='inner', axis=1)
        return f, train_y, test_f, test_y
