from utils.pgsql import CONN
import unicodecsv as csv
import pandas as pd


class PageViews:
    def __init__(self):
        pass

    def pgInit(self):
        data = []
        sql = "SELECT pl.week,pl.year,avg(pl.pv) as pv,avg(pl.uv) as uv,avg(pl.收藏数) as 收藏数,avg(pl.加购数) as 加购数"
        sql += ",AVG(平均停留时长) as 平均停留时长, sum(搜索引导访客数) as 搜索引导访客数 FROM "
        sql += "(SELECT extract (week from 日期)::integer as week,extract (YEAR from 日期)::integer as year,pv,uv,"
        sql += "收藏数,加购数,平均停留时长,搜索引导访客数 FROM 销售预测_流量数据 WHERE spu_tm ='AA000006') as pl"
        sql += " GROUP BY pl.week,pl.year ORDER BY pl.year,pl.week"
        cursor = CONN.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            data.append(PageViewsList(row))
        cursor.close()
        return data

    def pageViewsCsvInsert(self):
        sales = self.pgInit()
        salesHead = ('week', 'year', 'pv', 'uv', 'cv', 'jsv', 'avgt', 'sv')
        writer = csv.DictWriter(open('cache/pageViews.csv', 'wb'), salesHead)
        writer.writeheader()
        for s in sales:
            writer.writerow({
                'week': s.week,
                'year': s.year,
                'pv': s.pv,
                'uv': s.uv,
                'cv': s.cv,
                'jsv': s.jsv,
                'avgt': s.avgt,
                'sv': s.sv
            })

    def train_pd(self):
        sql = "SELECT pl.week,pl.year,pl.spu_tm,avg(pl.pv) as pv,avg(pl.uv) as uv,avg(pl.收藏数) as sc,avg(pl.加购数) as jg"
        sql += ",AVG(平均停留时长) as avgst, sum(搜索引导访客数) as se FROM "
        sql += "(SELECT extract (week from 日期)::integer as week,extract (YEAR from 日期)::integer as year,spu_tm,pv,uv,"
        sql += "收藏数,加购数,平均停留时长,搜索引导访客数 FROM 销售预测_流量数据_20200420) as pl"
        sql += " GROUP BY pl.week,pl.year,pl.spu_tm ORDER BY pl.year,pl.week"
        return pd.read_sql(sql, CONN)

    def daily_pd(self,code= "AA000006"):
        sql = "SELECT to_char(日期,'YYYYMMDD')::INTEGER AS days,to_char(日期,'MM-DD')AS day,EXTRACT(week FROM 日期)::INTEGER AS week"
        sql += ",EXTRACT(YEAR FROM 日期)::INTEGER AS YEAR,SUM(pv) AS pv,SUM(uv) AS uv,SUM (收藏数) AS sc,"
        sql += "SUM (加购数) AS jg,CASE WHEN AVG(平均停留时长)>0 THEN AVG(平均停留时长) ELSE 0 END AS at,"
        sql += "CASE WHEN SUM(搜索引导访客数)>0 THEN SUM(搜索引导访客数) ELSE 0 END AS sh FROM 销售预测_流量数据"
        sql += " WHERE	spu_tm = '"+code+"' GROUP BY days,week,YEAR,day ORDER BY days"
        return pd.read_sql(sql, CONN)


class PageViewsList:
    week = 0  # 周
    year = 0  # 年
    pv = 0
    uv = 0
    cv = 0  # 收藏
    jsv = 0  # 加入购物车
    avgt = 0  # 停留时长
    sv = 0  # 索引进入访客数

    def __init__(self, d):
        self.week = d[0]
        self.year = d[1]
        self.pv = d[2]
        self.uv = d[3]
        self.cv = d[4]
        self.jsv = d[5]
        self.avgt = d[6]
        self.sv = d[7]
