from utils.pgsql import CONN
import unicodecsv as csv
import numpy as np
import pandas as pd


class Sales:
    def __init__(self):
        pass

    def pgInit(self):
        data = []
        sql = "SELECT week,year,SUM ( 付款件数 ) as 件数,"
        sql += "avg(CASE WHEN 常规销售单价-单价>0 THEN 常规销售单价-单价 ELSE 0 END)::NUMERIC(19,2) as 平均优惠价格,"
        sql += "avg(单价)::NUMERIC(19,2) as 平均单价,"
        sql += "avg(CASE WHEN 常规销售单价-单价>0 THEN 1 ELSE 0 END)::NUMERIC(19,2) as 活动次数"
        sql += " FROM (SELECT EXTRACT( week FROM 日期 ):: INTEGER AS week,EXTRACT ( YEAR FROM 日期 ):: INTEGER AS year,"
        sql += "日期,店铺代码_tm,订单号_tm,sku_tm,	付款件数,付款金额,付款金额/付款件数 as 单价,常规销售单价,平台"
        sql += " FROM 销售预测_交易支付数据 WHERE"
        sql += " 订单状态 NOT LIKE '%拆单关闭,已作废,已取消%' AND sku_tm LIKE'%AA000006%' ORDER BY year,week,店铺代码_tm)"
        sql += " AS xl GROUP BY year,week"
        cursor = CONN.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            data.append(SalesList(row))
        cursor.close()
        return data

    def salesCsvInsert(self):
        sales = self.pgInit()
        salesHead = ('week', 'year', 'avgPrice', 'price', 'num', 'hd')
        writer = csv.DictWriter(open('cache/sales.csv', 'wb'), salesHead)
        writer.writeheader()
        for s in sales:
            writer.writerow({
                'week': s.week,
                'year': s.year,
                'avgPrice': s.avgPrice,
                'price': s.price,
                'num': s.num,
                'hd': s.hd
            })

    def salesData(self):
        data = []
        sql = "SELECT EXTRACT(week FROM 日期 )::INTEGER AS week,EXTRACT(MONTH FROM 日期 )::INTEGER AS month"
        sql += ",EXTRACT(YEAR FROM 日期 )::INTEGER AS YEAR,日期,店铺代码_tm,订单号_tm,sku_tm,"
        sql += "付款件数,付款金额,常规销售单价,平台,(付款金额/付款件数)::NUMERIC(19,2) as 单价,"
        sql += "(常规销售单价-(付款金额/付款件数)::NUMERIC(19,2))::NUMERIC(19,2) as 优惠金额"
        sql += " FROM 销售预测_交易支付数据 WHERE "
        sql += "订单状态 NOT LIKE '%拆单关闭,已作废,已取消%' AND sku_tm LIKE '%AA000006%' ORDER BY 日期"
        # pd_data = pd.read_sql(sql, CONN)
        # print(pd_data)
        cursor = CONN.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            data.append(SalesEntity(row))
        cursor.close()
        return data

    # 训练数据提取
    def train_data(self):
        data = self.salesData()
        for s in data:
            time = s.time.strftime('%Y%d-') + str(s.week)
            month = s.month
            unit_price = s.unitPrice
            num = s.num
            a_weeks_ago_num = 0
            two_weeks_ago_num = 0
            three_weeks_ago_num = 0

    def train_pd(self):
        sql = "SELECT week,year,sum(付款件数)::INTEGER as num,avg(单价)::NUMERIC(19,2)as avgPrice,"
        sql += "CASE WHEN avg(优惠金额)>0 THEN avg(优惠金额)::NUMERIC(19,2) ELSE 0 END as avgPD"
        sql += " FROM (SELECT EXTRACT(week FROM 日期 )::INTEGER AS week,"
        sql += "EXTRACT(MONTH FROM 日期 )::INTEGER AS month,	EXTRACT(YEAR FROM 日期 )::INTEGER AS YEAR,"
        sql += "EXTRACT(DAY FROM 日期 )::INTEGER AS day,to_char(日期, 'mm-dd') as mm,"
        sql += "日期,店铺代码_tm,订单号_tm,sku_tm,付款件数,付款金额,常规销售单价,平台,(付款金额/付款件数)::NUMERIC(19,2) AS 单价,"
        sql += "(常规销售单价-(付款金额/付款件数)::NUMERIC(19,2))::NUMERIC(19,2) AS 优惠金额"
        sql += " FROM 销售预测_交易支付数据 WHERE 订单状态 NOT LIKE '%拆单关闭,已作废,已取消%'"
        sql += " AND sku_tm LIKE '%AA000006%' ORDER BY 日期) as sl WHERE mm<>'11-11' GROUP BY year,week"
        return pd.read_sql(sql, CONN)

    # 每天数据
    def daily_pd(self, code='AA000006'):
        sql = "SELECT to_char(日期,'YYYYMMDD')::INTEGER AS days,to_char(日期,'MM-DD')AS DAY,EXTRACT(week FROM 日期)::INTEGER AS week"
        sql += ",EXTRACT(YEAR FROM 日期 )::INTEGER AS YEAR"
        sql += ",CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN " \
               "avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS discount,"
        sql += "avg( CASE WHEN (常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))>0 THEN 1 ELSE 0 END )::NUMERIC(19,2) as hd,"
        sql += "avg(CASE WHEN 常规销售单价-付款金额>0 THEN 常规销售单价-付款金额 ELSE 付款金额 END)::NUMERIC(19,2) as avgprice,"
        sql += "sum(付款件数) as num, substr(sku_tm, 0, 9) as spu FROM 销售预测_交易支付数据 "
        sql += "where sku_tm LIKE '%" + code + "%' AND 订单状态 NOT LIKE'%拆单关闭,已作废,已取消%' "
        sql += "GROUP BY 日期,spu ORDER BY 日期"
        pd_data = pd.read_sql(sql, CONN)
        pd_data.loc[lambda df: df.hd > 0, ('hd')] = [1]
        pd_data.loc[lambda df: df.discount < 0, ('discount')] = [0]
        return pd_data

    # 单个sku销量
    def week_sku_data(self, province=False):
        sql = "select sa.week,sa.year,sa.month,sum(sa.件数) as total,"
        if province:
            sql += "sa.省 as pro,"
        sql += "avg(sa.优惠金额)::NUMERIC(19,2) AS discount,"
        sql += "avg(sa.折扣)::NUMERIC(19,2) as dis,avg(sa.原价)::NUMERIC(19,2) as price,sku_tm,substr(sa.sku_tm,0," \
               "9) as spu_tm "
        sql += " FROM(SELECT to_char(日期, 'YYYY-MM-DD') AS days,to_char( 日期, 'MM-DD' ) AS DAY,"
        sql += "EXTRACT(week FROM 日期)::INTEGER AS week,EXTRACT(MONTH FROM 日期)::INTEGER AS month,"
        sql += "EXTRACT(YEAR FROM 日期)::INTEGER AS YEAR,"
        sql += "replace(replace(replace(replace(replace(replace(省,'省',''),'市',''),'广西壮族自治区','广西')," \
               "'新疆维吾尔自治区','新疆'),'宁夏回族自治区','宁夏'),'自治区','') as 省"
        sql += ",SUM(付款件数) AS 件数,sku_tm,substr(sku_tm, 0, 9) as spu,"
        sql += "CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN " \
               "avg((常规销售单价-(付款金额/付款件数)::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS 优惠金额,"
        sql += "AVG(CASE WHEN (常规销售单价- (付款金额/付款件数)::NUMERIC(19, 2)) >0 THEN 1 ELSE 0 END )::INTEGER AS hd,"
        sql += "AVG(((付款金额/付款件数)/常规销售单价)*10)::NUMERIC(19,2) as 折扣,avg(常规销售单价)::NUMERIC(19,2) as 原价"
        sql += " FROM 销售预测_交易支付数据_20200420 GROUP BY 日期,省,sku_tm) AS sa"
        sql += " GROUP BY sa.week,sa.year,sa.month,sa.sku_tm"
        if province:
            sql += ",sa.省"
        sql += " ORDER BY sa.year"
        pd_data = pd.read_sql(sql, CONN)
        return pd_data

    # spu销量
    def week_spu_data(self, province=False):
        sql = "select sa.week,sa.year,sa.month,sum(sa.件数) as total,"
        if province:
            sql += "sa.省 as pro,"
        sql += "avg(sa.优惠金额)::NUMERIC(19,2) AS discount,"
        sql += "avg(sa.折扣)::NUMERIC(19,2) as dis,avg(sa.原价)::NUMERIC(19,2) as price,sa.spu_tm"
        sql += " FROM(SELECT to_char(日期, 'YYYY-MM-DD') AS days,to_char( 日期, 'MM-DD' ) AS DAY,"
        sql += "EXTRACT(week FROM 日期)::INTEGER AS week,EXTRACT(MONTH FROM 日期)::INTEGER AS month,"
        sql += "EXTRACT(YEAR FROM 日期)::INTEGER AS YEAR,"
        sql += "replace(replace(replace(replace(replace(replace(省,'省',''),'市',''),'广西壮族自治区','广西')," \
               "'新疆维吾尔自治区','新疆'),'宁夏回族自治区','宁夏'),'自治区','') as 省"
        sql += ",SUM(付款件数) AS 件数,sku_tm,substr(sku_tm, 0, 9) as spu_tm,"
        sql += "CASE WHEN (avg((常规销售单价-(付款金额/付款件数 )::NUMERIC(19,2))))>0 THEN " \
               "avg((常规销售单价-(付款金额/付款件数)::NUMERIC(19,2)))::NUMERIC(19,2) ELSE 0 END AS 优惠金额,"
        sql += "AVG(CASE WHEN (常规销售单价- (付款金额/付款件数)::NUMERIC(19, 2)) >0 THEN 1 ELSE 0 END )::INTEGER AS hd,"
        sql += "AVG(((付款金额/付款件数)/常规销售单价)*10)::NUMERIC(19,2) as 折扣,avg(常规销售单价)::NUMERIC(19,2) as 原价"
        sql += " FROM 销售预测_交易支付数据_20200420 GROUP BY 日期,省,sku_tm) AS sa"
        sql += " GROUP BY sa.week,sa.year,sa.month,sa.spu_tm"
        if province:
            sql += ",sa.省"
        sql += " ORDER BY sa.year"
        pd_data = pd.read_sql(sql, CONN)
        return pd_data


class SalesList:
    week = 0  # 周
    year = 0  # 年
    avgPrice = 0.00  # 平均优惠价格
    price = 0.00  # 平均单价
    num = 0
    hd = 0

    def __init__(self, data):
        self.week = data[0]
        self.year = data[1]
        self.num = data[2]
        self.avgPrice = data[3]
        self.price = data[4]
        self.hd = data[5]
        pass


# 销售原数据
class SalesEntity:
    week = 0  # 周
    month = 0  # 月
    year = 0  # 年
    time = ""  # 日期
    seller = 0  # 店铺ID
    order = 0  # 订单ID
    sku = 0
    num = 0  # 购买数量
    payment = 0  # 付款金额
    price = 0.00  # 价格
    pt = ""  # 平台
    unitPrice = 0.00  # 单价
    discount = 0.00  # 优惠金额

    def __init__(self, data):
        self.week = data[0]  # 周
        self.month = data[1]  # 月
        self.year = data[2]  # 年
        self.time = data[3]  # 日期
        self.seller = data[4]
        self.order = data[5]
        self.sku = data[6]
        self.num = data[7]
        self.payment = data[8]
        self.price = data[9]
        self.pt = data[10]
        self.unitPrice = data[11]
        self.discount = data[12]


# 训练数据
class TrainSales:
    # Before
    time = ""  # 年月-周
    month = 0  # 月
    unitPrice = 0.00  # 单价
    num = 0  # 购买数量
    aWeeksAgoNum = 0  # 一周前
    twoWeeksAgoNum = 0  # 二周前
    threeWeeksAgoNum = 0  # 三周前
    promotion = 0  # 促销次数

    def __init__(self):
        pass
